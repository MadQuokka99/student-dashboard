import React, { useCallback, useEffect, useState } from "react";
import { SemanticToastContainer } from "react-semantic-toasts";
import {
  Menu,
  Segment,
  Grid,
  Icon,
  Image,
  Header,
  Label,
} from "semantic-ui-react";
import { GridWithTransition } from "../Components/PaginaSetari/PaginaSetariElements";
import SetariAvatar from "../Components/PaginaSetari/SetariAvatar";
import SetariCantina from "../Components/PaginaSetari/SetariCantina";
import SetariFacultate from "../Components/PaginaSetari/SetariFacultate";
import {
  setariDashboardStudentUpdate,
  setariDashboardStudentGetByStudent,
  anUniversitarCurentGet,
  studentInfo,
} from "../Utils/ApiCalls";
import { debounce, showToast } from "../Utils/Functions";
import "react-semantic-toasts/styles/react-semantic-alert.css";
import { getUsername } from "../Utils/GetUsername";
import { Link } from "react-router-dom";

const SettingsPage = () => {
  const [activeItem, setActiveItem] = useState("avatar");
  const [screenWidth, setScreenWidth] = useState(window.innerWidth);
  const [setariDashboardStudent, setSetariDashboardStudent] = useState(null);
  const [optiuniFacultate, setOptiuniFacultate] = useState([]);
  const [facultateAleasa, setFacultateAleasa] = useState(null);
  const [avatarStudent, setAvatarStudent] = useState("");

  const username = getUsername();

  const handleItemClick = (e, { name }) => {
    setActiveItem(name);
  };

  const fetchData = useCallback(async () => {
    let setari = null;
    const setariDashboardStudentResponse =
      await setariDashboardStudentGetByStudent();
    setari = setariDashboardStudentResponse.data;

    setSetariDashboardStudent(setari);

    const ID_AnUnivCurent = await anUniversitarCurentGet();
    const studentInfoResponse = await studentInfo(
      username,
      ID_AnUnivCurent.data.ID_AnUniv
    );

    let facultatiStudent = [];

    for (const stud of studentInfoResponse.data) {
      facultatiStudent.push({
        value: stud.ID_Facultate + stud.ID_Specializare,
        text: stud.DenumireFacultate + " - " + stud.DenumireSpecializare,
        key: stud.ID_Facultate + stud.ID_Specializare,
        info: stud,
      });
    }

    setOptiuniFacultate(facultatiStudent);

    let facultate = null;
    const facultateDinSetari = facultatiStudent.filter(
      (fac) => fac.info.ID_Student === setari.ID_Student
    )[0];

    facultate = facultateDinSetari;

    setFacultateAleasa(facultate);

    if (setari !== null) {
      setAvatarStudent(setari.SetariAvatar.LinkAvatar);
    }
  }, [username]);

  const updateSetariDashboardStudent = async (setari) => {
    const setariResponse = await setariDashboardStudentUpdate(setari);
    setSetariDashboardStudent(setariResponse.data);
    setAvatarStudent(setariResponse.data.SetariAvatar.LinkAvatar);
    const facultateDinSetari = optiuniFacultate.filter(
      (fac) => fac.info.ID_Student === setari.ID_Student
    )[0];
    setFacultateAleasa(facultateDinSetari);
    showToast(false, "Setările au fost salvate cu succes!");
  };

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  /**
   * functia debouncedHandleResize este apelata o data la 1 sec pentru a actualiza latimea ecranului
   * functia se apeleaza atunci cand ecranul este redimensionat
   */
  useEffect(() => {
    const debouncedHandleResize = debounce(function handleResize() {
      setScreenWidth(window.innerWidth);
    }, 1000);

    window.addEventListener("resize", debouncedHandleResize);
    return (_) => {
      window.removeEventListener("resize", debouncedHandleResize);
    };
  });

  return (
    <div>
      {screenWidth > 600 ? (
        <>
          <GridWithTransition>
            <Grid.Row
              columns={3}
              style={{
                margin: "1em 0 0",
              }}
            >
              <Grid.Column textAlign="left">
                <Link to="/">
                  <Label size="big">👈🏻 Dashboard</Label>
                </Link>
              </Grid.Column>
              <Grid.Column>
                <Header content="Setări dashboard" as="h1" />
              </Grid.Column>
              <Grid.Column style={{ display: "flex", justifyContent: "right" }}>
                <Link to="/">
                  <Image
                    style={{
                      cursor: "pointer",
                    }}
                    src={avatarStudent}
                    size="tiny"
                    circular
                  />
                </Link>
              </Grid.Column>
            </Grid.Row>
            <Grid.Column width={4}>
              <Menu fluid vertical tabular>
                <Menu.Item
                  name="avatar"
                  active={activeItem === "avatar"}
                  onClick={handleItemClick}
                />
                <Menu.Item
                  name="facultate"
                  active={activeItem === "facultate"}
                  onClick={handleItemClick}
                />
                <Menu.Item
                  name="cantina"
                  active={activeItem === "cantina"}
                  onClick={handleItemClick}
                />
              </Menu>
            </Grid.Column>

            <Grid.Column stretched width={12}>
              <Segment style={{ padding: "2em 0" }}>
                {activeItem === "facultate" ? (
                  <SetariFacultate
                    setari={setariDashboardStudent}
                    updateSetari={updateSetariDashboardStudent}
                    optiuniFacultate={optiuniFacultate}
                    facultateAleasa={facultateAleasa}
                  />
                ) : activeItem === "cantina" ? (
                  <SetariCantina
                    setari={setariDashboardStudent}
                    updateSetari={updateSetariDashboardStudent}
                  />
                ) : activeItem === "avatar" ? (
                  <SetariAvatar
                    setari={setariDashboardStudent}
                    updateSetari={updateSetariDashboardStudent}
                    facultateAleasa={facultateAleasa}
                  />
                ) : null}
              </Segment>
            </Grid.Column>
          </GridWithTransition>
          <SemanticToastContainer />
        </>
      ) : (
        <>
          <Menu icon>
            <Link to="/">
              <Menu.Item name="back" active={activeItem === "back"}>
                <Icon name="arrow left" />
              </Menu.Item>
            </Link>
            <Menu.Item
              name="avatar"
              active={activeItem === "avatar"}
              onClick={handleItemClick}
            >
              <Icon name="user outline" />
            </Menu.Item>
            <Menu.Item
              name="facultate"
              active={activeItem === "facultate"}
              onClick={handleItemClick}
            >
              <Icon name="building outline" />
            </Menu.Item>

            <Menu.Item
              name="cantina"
              active={activeItem === "cantina"}
              onClick={handleItemClick}
            >
              <Icon name="food" />
            </Menu.Item>

            <Link to="/">
              <Menu.Item style={{ display: "flex", justifyContent: "right" }}>
                <Image src={avatarStudent} size="mini" circular />
              </Menu.Item>
            </Link>
          </Menu>

          <Header
            content="Setări dashboard"
            as="h2"
            style={{
              margin: "1em 0em 1.5em",
              textAlign: "center"
            }}
          />

          <Segment style={{ padding: "2em 0" }}>
            {activeItem === "facultate" ? (
              <SetariFacultate
                setari={setariDashboardStudent}
                updateSetari={updateSetariDashboardStudent}
                optiuniFacultate={optiuniFacultate}
                facultateAleasa={facultateAleasa}
              />
            ) : activeItem === "cantina" ? (
              <SetariCantina
                setari={setariDashboardStudent}
                updateSetari={updateSetariDashboardStudent}
              />
            ) : activeItem === "avatar" ? (
              <SetariAvatar
                setari={setariDashboardStudent}
                updateSetari={updateSetariDashboardStudent}
                facultateAleasa={facultateAleasa}
              />
            ) : null}
          </Segment>
          <SemanticToastContainer />
        </>
      )}
    </div>
  );
};

export default SettingsPage;
