import React, { useState, useCallback, useEffect } from "react";
import {
  Grid,
  Header,
  Image,
  Container,
  Menu,
  Accordion,
  Icon,
  Label,
} from "semantic-ui-react";
import {
  SegmentComponent,
  IframeTutorialeYoutube,
  DivTutorialeYoutube,
  HeaderComponent,
  DivWithTransition,
} from "../Components/PaginaDashboard/DashboardPageElements";

import AvatarCardComponent from "../Components/PaginaDashboard/AvatarCardComponent";
import ErrorMessageComponent from "../Components/GeneralElements/ErrorMessageComponent";
import LoaderMessageComponentElement from "../Components/GeneralElements/LoaderMessageComponentElement";

import { Link } from "react-router-dom";

import {
  setariDashboardStudentGetByStudent,
  studentInfo,
  anUniversitarCurentGet,
  setariDashboardStudentUpdate,
} from "../Utils/ApiCalls";
import { getUsername } from "../Utils/GetUsername";
import { getFacultateColor } from "../Utils/GetStyleByID_Departament";
import { getKeysOfObject, debounce } from "../Utils/Functions";
import Sigla from "../Utils/Sigla";
import CantinaComponent from "../Components/PaginaDashboard/Cantina/CantinaComponent";
import TitluSectiune from "../Components/PaginaDashboard/TitluSectiune";
import NoteComponent from "../Components/PaginaDashboard/Note/NoteComponent";
import OrarSiProgramareaExamenelorComponent from "../Components/PaginaDashboard/Orar si Programarea Examenelor/OrarSiProgramareaExamenelorComponent";
import BurseComponent from "../Components/PaginaDashboard/Burse/BurseComponent";
import NotificariComponent from "../Components/PaginaDashboard/Notificari/NotificariComponent";
import AnunturiComponent from "../Components/PaginaDashboard/Anunturi/AnunturiComponent";
import PlatformeUnitbvComponent from "../Components/PaginaDashboard/PlatformeUNITBV/PlatformeUnitbvComponent";
import AnunturiIntranetComponent from "../Components/PaginaDashboard/AnunturiIntranet/AnunturiIntranetComponent";
import SportNeprofilComponent from "../Components/PaginaDashboard/SportNeprofil/SportNeprofilComponent";
import MediiComponent from "../Components/PaginaDashboard/Medii/MediiComponent";
import LaIndemanaComponent from "../Components/PaginaDashboard/La indemana/LaIndemanaComponent";
import TaxeComponent from "../Components/PaginaDashboard/Taxe/TaxeComponent";
import TurVirtualComponent from "../Components/PaginaDashboard/Tur virtual/TurVirtualComponent";

const setariDashboardObject = {
  ID_Facultate: null,
  ID_Departament: null,
  ID_Student: null,
  ID_AnUniv: null,
  CantinaPreferata: null,
  Subgrupa: "A",
  SetariAvatar: {
    HairStyleProbability: 50,
    BackgroundColor: null,
    AccessoriesProbability: 50,
    FacialHairProbability: 40,
    Eyes: ["happy"],
    EyeBrow: ["raisedExcited"],
    Mouth: ["smile"],
    Sex: null,
  },
};

const DashboardPage = () => {
  const [setariDashboardStudent, setSetariDashboardStudent] = useState(null);
  const [facultateaAleasa, setFacultateaAleasa] = useState(null);
  const [avatarStudent, setAvatarStudent] = useState("");
  const [ID_DepartamentFacultateAleasa, setID_DepartamentFacultateAleasa] =
    useState(null);
  const [denumireCantina, setDenumireCantina] = useState("");

  const [activeIndex, setActiveIndex] = useState(1);

  const [screenWidth, setScreenWidth] = useState(window.innerWidth);
  const [error, setError] = useState(null); //folosit pentru afisarea mesajului de eroare daca nu vin datele de afisat
  const [isLoading, setIsLoading] = useState(false);

  const username = getUsername();

  /**
   * functia debouncedHandleResize este apelata o data la 1 sec pentru a actualiza latimea ecranului
   * functia se apeleaza atunci cand ecranul este redimensionat
   */
  useEffect(() => {
    const debouncedHandleResize = debounce(function handleResize() {
      setScreenWidth(window.innerWidth);
    }, 1000);

    window.addEventListener("resize", debouncedHandleResize);
    return (_) => {
      window.removeEventListener("resize", debouncedHandleResize);
    };
  });

  const fetchData = useCallback(async () => {
    setIsLoading(true);
    setError(null);

    try {
      //flag pentru a sti daca se actualizeaza sau nu informatiile studentului
      //ex situatie: studentul avea setata facultatea 2 si s-a retras de la 2 si a ramas doar la 1, cand va intra ii va da eroare
      //pt ca ID_Student, ID_Facultate etc nu vor fi nule, deci nu se va face update, si se va face call cu id-ul facultatii 2, la care nu mai este
      let idStudentSeAflaInListaDeFacultatiStudent = true;

      let setari = null;
      const setariDashboardStudent = await setariDashboardStudentGetByStudent();
      setari = setariDashboardStudent.data;

      const ID_AnUnivCurent = await anUniversitarCurentGet();
      const studentInfoResponse = await studentInfo(
        username,
        ID_AnUnivCurent.data.ID_AnUniv
      );

      let facultatiStudent = [];

      for (const stud of studentInfoResponse.data) {
        facultatiStudent.push({
          value: stud.ID_Facultate + stud.ID_Specializare,
          text: stud.DenumireFacultate + " - " + stud.DenumireSpecializare,
          key: stud.ID_Facultate + stud.ID_Specializare,
          info: stud,
        });
      }

      let facultate = null;

      if (
        setari === null ||
        setari.ID_Student === 0 ||
        setari.ID_Student === null
      ) {
        facultate = facultatiStudent[0];
      } else {
        const facultateDinSetari = facultatiStudent.filter(
          (fac) => fac.info.ID_Student === setari.ID_Student
        )[0];

        if (facultateDinSetari === undefined) {
          idStudentSeAflaInListaDeFacultatiStudent = false;
          facultate = facultatiStudent[0];
        } else {
          facultate = facultateDinSetari;
        }
      }

      setFacultateaAleasa(facultate);
      setID_DepartamentFacultateAleasa(facultate.info.ID_Departament);

      const culoareFacultate = getFacultateColor(facultate.info.ID_Departament);

      const culoare = culoareFacultate.substr(1, 6);
      const culoareBackground = "%23" + culoare;

      let numeIntregUser = facultate.info.NumeIntreg;
      let sexUser = facultate.info.Sex === "F" ? "female" : "male";

      if (
        setari === null ||
        setari.ID_Student === 0 ||
        setari.ID_Student === null ||
        setari.ID_Facultate === 0 ||
        setari.ID_Facultate === null ||
        setari.ID_Departament === 0 ||
        setari.ID_Departament === null ||
        setari.SetariAvatar.BackgroundColor === "" ||
        setari.SetariAvatar.Sex === null ||
        setari.CantinaPreferata === null ||
        setari.ID_AnUniv === null ||
        setari.ID_AnUniv === 0 ||
        setari.Subgrupa === "" ||
        !idStudentSeAflaInListaDeFacultatiStudent
      ) {
        const setariDashboardInitialObj = { ...setariDashboardObject };
        setariDashboardInitialObj.ID_Facultate = facultate.value;
        setariDashboardInitialObj.ID_Student = facultate.info.ID_Student;
        setariDashboardInitialObj.ID_AnUniv = ID_AnUnivCurent.data.ID_AnUniv;
        setariDashboardInitialObj.CantinaPreferata =
          setari === null ||
          setari.CantinaPreferata === "" ||
          setari.CantinaPreferata === 0 ||
          setari.CantinaPreferata === null
            ? "memo"
            : setari.CantinaPreferata;
        setariDashboardInitialObj.SetariAvatar.BackgroundColor =
          culoareBackground;
        setariDashboardInitialObj.Subgrupa =
          setari === null ||
          setari.Subgrupa === "" ||
          setari.Subgrupa === 0 ||
          setari.Subgrupa === null
            ? "A"
            : setari.Subgrupa;
        setariDashboardInitialObj.ID_Departament =
          facultate.info.ID_Departament;

        setariDashboardInitialObj.SetariAvatar.Sex = sexUser;

        const setariUpdated = await setariDashboardStudentUpdate(
          setariDashboardInitialObj
        );

        setari = setariUpdated.data;
      }

      setSetariDashboardStudent(setari);
      let profil = "";

      let objSetariAvatar = {
        top: setari.SetariAvatar.HairStyle,
        topChance: setari.SetariAvatar.HairStyleProbability,
        hairColor: setari.SetariAvatar.HairColor,
        accessories: setari.SetariAvatar.Accessories,
        accessoriesChance: setari.SetariAvatar.AccessoriesProbability,
        facialHair: setari.SetariAvatar.FacialHair,
        facialHairChance: setari.SetariAvatar.FacialHairProbability,
        facialHairColor: setari.SetariAvatar.FacialHairColor,
        clothes: setari.SetariAvatar.Clothes,
        clothesColor: setari.SetariAvatar.ClothesColor,
        eyes: setari.SetariAvatar.Eyes,
        eyebrow: setari.SetariAvatar.EyeBrow,
        mouth: setari.SetariAvatar.Mouth,
        skin: setari.SetariAvatar.Skin,
      };

      let baseURL = `https://avatars.dicebear.com/api/avataaars/${sexUser}/:${numeIntregUser.trim()}.svg?backgroundColor=${culoareBackground}&scale=100`;

      const listaCampuriObjSetari = getKeysOfObject(objSetariAvatar);
      profil = baseURL;

      listaCampuriObjSetari.forEach((element) => {
        if (objSetariAvatar[element] !== null) {
          if (Array.isArray(objSetariAvatar[element])) {
            profil = profil.concat(
              `&${element}[]=${objSetariAvatar[element][0]}`
            );
          } else {
            profil = profil.concat(`&${element}=${objSetariAvatar[element]}`);
          }
        }
      });

      setari.SetariAvatar.LinkAvatar = profil;

      const setariUpdatedBD = await setariDashboardStudentUpdate(setari);
      setSetariDashboardStudent(setariUpdatedBD.data);

      setAvatarStudent(profil);

      if (setari.CantinaPreferata === "memo") {
        setDenumireCantina("Cantină Memo");
      } else if (setari.CantinaPreferata === "colina") {
        setDenumireCantina("Cantină Colină");
      }
    } catch (error) {
      setError(error.response);
    }

    setIsLoading(false);
  }, [username]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const trimiteCatrePaginaCantina = (e) => {
    const url = "https://intranet.unitbv.ro/Facultate/Meniu-Cantina";
    window.open(url, "_black");
  };

  const trimiteCatrePaginaNote = (e) => {
    const url = "https://intranet.unitbv.ro/Facultate/Notele-si-bursele-mele";
    window.open(url, "_black");
  };

  const trimiteCatrePaginaAnunturi = (e) => {
    const url = "https://intranet.unitbv.ro/Secretariat/Avizier-facultate";
    window.open(url, "_black");
  };

  const trimiteCatrePaginaAlegeSport = (e) => {
    const url = "https://intranet.unitbv.ro/Facultate/Alege-sport";
    window.open(url, "_black");
  };

  const trimiteCatrePaginaTaxe = (e) => {
    const url = "https://intranet.unitbv.ro/Facultate/Taxele-mele";
    window.open(url, "_black");
  };

  const handleClick = (e, titleProps) => {
    const { index } = titleProps;
    const newIndex = activeIndex === index ? -1 : index;

    setActiveIndex(newIndex);
  };

  const calendarView = (
    <SegmentComponent height={"400px"}>
      <OrarSiProgramareaExamenelorComponent
        setari={
          setariDashboardStudent === undefined
            ? setariDashboardObject
            : setariDashboardStudent
        }
        studentInfo={
          facultateaAleasa !== null ? facultateaAleasa.info : undefined
        }
        screenWidth={screenWidth}
      />
    </SegmentComponent>
  );

  const cantinaView = (
    <SegmentComponent height={"400px"} onClick={trimiteCatrePaginaCantina}>
      <CantinaComponent
        setari={setariDashboardStudent}
        screenWidth={screenWidth}
      />
    </SegmentComponent>
  );

  const noteView = (
    <SegmentComponent height={"300px"} onClick={trimiteCatrePaginaNote}>
      <NoteComponent
        setari={setariDashboardStudent}
        screenWidth={screenWidth}
      />
    </SegmentComponent>
  );

  const laIndemanaVView = (
    <SegmentComponent height={"300px"}>
      <LaIndemanaComponent screenWidth={screenWidth} />
    </SegmentComponent>
  );

  const tutorialeView = (
    <SegmentComponent>
      <DivTutorialeYoutube>
        <IframeTutorialeYoutube
          src="https://www.youtube.com/embed/videoseries?list=PL3940Gzbc2A4EVTaxPWZ1HF1b39Kdm4E5"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></IframeTutorialeYoutube>
      </DivTutorialeYoutube>
    </SegmentComponent>
  );

  const burseView = (
    <SegmentComponent height={"300px"} onClick={trimiteCatrePaginaNote}>
      <BurseComponent
        setari={setariDashboardStudent}
        screenWidth={screenWidth}
      />
    </SegmentComponent>
  );

  const notificariView = (
    <SegmentComponent height={"300px"}>
      <NotificariComponent setari={setariDashboardStudent} />
    </SegmentComponent>
  );

  const anunturiUNITBVView = (
    <SegmentComponent height={"400px"} style={{ textAlign: "center" }}>
      <AnunturiComponent
        setari={setariDashboardStudent}
        ID_Departament={null}
      />
    </SegmentComponent>
  );

  const anunturiFacultateView = (
    <SegmentComponent height={"400px"} style={{ textAlign: "center" }}>
      <AnunturiComponent
        setari={setariDashboardStudent}
        ID_Departament={ID_DepartamentFacultateAleasa}
      />
    </SegmentComponent>
  );

  const platformeleUNITBVView = (
    <SegmentComponent height={"400px"}>
      <PlatformeUnitbvComponent screenWidth={screenWidth} />
    </SegmentComponent>
  );

  const anunturiIntranetView = (
    <SegmentComponent
      height={"400px"}
      onClick={trimiteCatrePaginaAnunturi}
      style={{ textAlign: "center" }}
    >
      <AnunturiIntranetComponent studentInfo={facultateaAleasa} />
    </SegmentComponent>
  );

  const sportNeprofilView = (
    <SegmentComponent height={"300px"} onClick={trimiteCatrePaginaAlegeSport}>
      <SportNeprofilComponent studentInfo={facultateaAleasa} />
    </SegmentComponent>
  );

  const mediiView = (
    <SegmentComponent height={"300px"} onClick={trimiteCatrePaginaNote}>
      <MediiComponent studentInfo={facultateaAleasa} />
    </SegmentComponent>
  );

  const taxeView = (
    <SegmentComponent height={"300px"} onClick={trimiteCatrePaginaTaxe}>
      <TaxeComponent studentInfo={facultateaAleasa} />
    </SegmentComponent>
  );

  const turVirtualView = (
    <SegmentComponent height={"300px"}>
      <TurVirtualComponent />
    </SegmentComponent>
  );

  let content = null;

  if (screenWidth < 600) {
    content = (
      <Accordion styled>
        <Grid columns={3} doubling stackable>
          <Grid.Row columns={3} style={{ marginTop: "1em" }}>
            <Grid.Column only="mobile">
              <Menu secondary>
                <Link to="/">
                  <Menu.Item>
                    <Sigla icon={ID_DepartamentFacultateAleasa} size={"40"} />
                  </Menu.Item>
                </Link>
                <Link to="/setari">
                  <Menu.Item
                    style={{ display: "flex", justifyContent: "right" }}
                  >
                    <Image src={avatarStudent} size="mini" circular />
                  </Menu.Item>
                </Link>

                <Link to="/setari">
                  <Menu.Item position="right">
                    <Label
                      circular
                      as="a"
                      size="large"
                      style={{ backgroundColor: "#D9D9D9", color: "#676767" }}
                    >
                      <Icon name="settings" /> Setări
                    </Label>
                  </Menu.Item>
                </Link>
              </Menu>
            </Grid.Column>
            <Grid.Column>
              <Header
                as="h2"
                style={{
                  margin: "0em 0em 1em",
                  textAlign: "center",
                }}
              >
                Universitatea Transilvania din Brașov
              </Header>
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <Accordion.Title
          active={activeIndex === 1}
          index={1}
          onClick={handleClick}
          key={1}
        >
          <Icon name="dropdown" />
          🔔 Orarul și examenele
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 1}>
          {calendarView}
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 2}
          index={2}
          onClick={handleClick}
          key={2}
        >
          <Icon name="dropdown" />
          {`🥪 Meniu ${denumireCantina}`}
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 2}>
          {cantinaView}
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 3}
          index={3}
          onClick={handleClick}
          key={3}
        >
          <Icon name="dropdown" />
          <Sigla icon={"unitbv"} size={"small"} /> Știri și evenimente UNITBV
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 3}>
          {anunturiUNITBVView}
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 4}
          index={4}
          onClick={handleClick}
          key={4}
        >
          <Icon name="dropdown" />
          <Sigla
            icon={
              setariDashboardStudent === null
                ? "unitbv"
                : ID_DepartamentFacultateAleasa
            }
            size={setariDashboardStudent === null ? "small" : "20"}
          />{" "}
          Știri și evenimente facultate
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 4}>
          {anunturiFacultateView}
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 5}
          index={5}
          onClick={handleClick}
          key={5}
        >
          <Icon name="dropdown" />
          <Sigla icon={"unitbv"} size={"small"} /> Anunțuri Intranet
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 5}>
          {anunturiIntranetView}
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 6}
          index={6}
          onClick={handleClick}
          key={6}
        >
          <Icon name="dropdown" />
          <Sigla icon={"unitbv"} size={"small"} /> Platformele UNITBV
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 6}>
          {platformeleUNITBVView}
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 7}
          index={7}
          onClick={handleClick}
          key={7}
        >
          <Icon name="dropdown" />
          💯 Note
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 7}>
          {noteView}
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 8}
          index={8}
          onClick={handleClick}
          key={8}
        >
          <Icon name="dropdown" />
          💰 Burse
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 8}>
          {burseView}
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 9}
          index={9}
          onClick={handleClick}
          key={9}
        >
          <Icon name="dropdown" />
          💸 Taxe
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 9}>
          {taxeView}
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 10}
          index={10}
          onClick={handleClick}
          key={10}
        >
          <Icon name="dropdown" />
          📱 Notificări Student@UNITBV
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 10}>
          {notificariView}
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 11}
          index={11}
          onClick={handleClick}
          key={11}
        >
          <Icon name="dropdown" />
          🧯 La îndemână
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 11}>
          {laIndemanaVView}
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 12}
          index={12}
          onClick={handleClick}
          key={12}
        >
          <Icon name="dropdown" />
          🏸 Sportul ales
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 12}>
          {sportNeprofilView}
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 13}
          index={13}
          onClick={handleClick}
          key={13}
        >
          <Icon name="dropdown" />
          🥇 Medii
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 13}>
          {mediiView}
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 14}
          index={14}
          onClick={handleClick}
          key={14}
        >
          <Icon name="dropdown" />
          👀 Tutoriale UNITBV
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 14}>
          {tutorialeView}
        </Accordion.Content>

        <Accordion.Title
          active={activeIndex === 16}
          index={16}
          onClick={handleClick}
          key={16}
        >
          <Icon name="dropdown" />
          📍 Tur virtual UNITBV
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 16}>
          {turVirtualView}
        </Accordion.Content>
      </Accordion>
    );
  } else
    content = (
      <DivWithTransition>
        <Grid columns={3} doubling stackable>
          <Grid.Row columns={3} style={{ marginTop: "1em" }}>
            <Grid.Column only="mobile">
              <Menu secondary>
                <Link to="/">
                  <Menu.Item>
                    <Sigla icon={ID_DepartamentFacultateAleasa} size={"40"} />
                  </Menu.Item>
                </Link>
                <Link to="/setari">
                  <Menu.Item position="right">
                    <Image src={avatarStudent} size="mini" circular />
                  </Menu.Item>
                </Link>
                <Link to="/setari">
                  <Menu.Item position="right">
                    <Label
                      circular
                      size="large"
                      style={{ backgroundColor: "#D9D9D9", color: "#676767" }}
                    >
                      <Icon name="settings" /> Setări
                    </Label>
                  </Menu.Item>
                </Link>
              </Menu>
            </Grid.Column>
            <Grid.Column verticalAlign="middle">
              <HeaderComponent
                as="h2"
                style={{
                  margin: "0em 0em 1.5em",
                  textAlign: "center",
                }}
              >
                Universitatea Transilvania din Brașov
              </HeaderComponent>
            </Grid.Column>
            <Grid.Column only="computer"></Grid.Column>
            <Grid.Column only="computer tablet">
              <Container>
                <SegmentComponent
                  compact
                  basic
                  style={{ display: "flex", justifyContent: "center" }}
                >
                  <AvatarCardComponent
                    avatarSrc={avatarStudent}
                    facultateaAleasa={facultateaAleasa}
                  />
                </SegmentComponent>
              </Container>
            </Grid.Column>
          </Grid.Row>

          <Grid.Column>
            <TitluSectiune titlu={"🔔 Orarul și examenele"} />
            {calendarView}
          </Grid.Column>

          <Grid.Column>
            <TitluSectiune
              titlu={`🥪 Meniu ${denumireCantina}`}
              screenWidth={screenWidth}
            />
            {cantinaView}
          </Grid.Column>

          <Grid.Column>
            <TitluSectiune
              titlu={` Știri și evenimente UNITBV`}
              screenWidth={screenWidth}
              sigla={<Sigla icon={"unitbv"} size={"small"} />}
            />
            {anunturiUNITBVView}
          </Grid.Column>

          <Grid.Column>
            <TitluSectiune
              titlu={` Știri și evenimente facultate`}
              screenWidth={screenWidth}
              sigla={
                <Sigla
                  icon={
                    setariDashboardStudent === null
                      ? "unitbv"
                      : ID_DepartamentFacultateAleasa
                  }
                  size={setariDashboardStudent === null ? "small" : "20"}
                />
              }
            />
            {anunturiFacultateView}
          </Grid.Column>

          <Grid.Column>
            <TitluSectiune
              titlu={` Anunțuri Intranet`}
              screenWidth={screenWidth}
              sigla={<Sigla icon={"unitbv"} size={"small"} />}
            />
            {anunturiIntranetView}
          </Grid.Column>

          <Grid.Column>
            <TitluSectiune
              titlu={" Platformele UNITBV"}
              sigla={<Sigla icon={"unitbv"} size={"small"} />}
            />
            {platformeleUNITBVView}
          </Grid.Column>

          <Grid.Column>
            <TitluSectiune titlu={`💯 Note`} screenWidth={screenWidth} />
            {noteView}
          </Grid.Column>

          <Grid.Column>
            <TitluSectiune titlu={`💰 Burse`} />
            {burseView}
          </Grid.Column>

          <Grid.Column>
            <TitluSectiune titlu={`💸 Taxe`} />
            {taxeView}
          </Grid.Column>

          <Grid.Column>
            <TitluSectiune
              titlu={`📱 Notificări Student@UNITBV`}
              screenWidth={screenWidth}
            />
            {notificariView}
          </Grid.Column>

          <Grid.Column>
            <TitluSectiune titlu={"🧯 La îndemână"} />
            {laIndemanaVView}
          </Grid.Column>

          <Grid.Column>
            <TitluSectiune titlu={"🏸 Sportul ales"} />
            {sportNeprofilView}
          </Grid.Column>
          <Grid.Column>
            <TitluSectiune titlu={"🥇 Medii"} />
            {mediiView}
          </Grid.Column>

          <Grid.Column>
            <TitluSectiune titlu={"👀 Tutoriale UNITBV"} />
            {tutorialeView}
          </Grid.Column>
          <Grid.Column>
            <TitluSectiune titlu={"📍 Tur virtual UNITBV"} />
            {turVirtualView}
          </Grid.Column>
        </Grid>
      </DivWithTransition>
    );

  if (error) {
    content = (
      <div style={{ marginTop: "4rem" }}>
        <ErrorMessageComponent error={error} />
      </div>
    );
  }

  if (isLoading) {
    content = (
      <div style={{ marginTop: "4rem" }}>
        <LoaderMessageComponentElement />
      </div>
    );
  }

  return content;
};

export default DashboardPage;
