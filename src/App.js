import "./App.css";
import "semantic-ui-css/semantic.min.css";
import DashboardPage from "./Page/DashboardPage";
import { Switch, Route, HashRouter } from "react-router-dom";
import SettingsPage from "./Page/SettingsPage";

function App() {
  return (
    <div className="App">
      <HashRouter>
        <Switch>
          <Route
            path="/"
            exact
            render={() => {
              return <DashboardPage />;
            }}
          />
          <Route
            path="/setari"
            render={() => {
              return <SettingsPage />;
            }}
          />
        </Switch>
      </HashRouter>
    </div>
  );
}

export default App;
