import React from "react";
import { Message } from "semantic-ui-react";

const ErrorMessageComponent = (props) => {
  return (
    <div>
      <Message negative>
        <Message.Header>Ceva nu a funcționat!</Message.Header>
        <p style={{ whiteSpace: "pre-wrap" }}>{props.error.Message}</p>
      </Message>
    </div>
  );
};

export default ErrorMessageComponent;
