import React from "react";
import { Loader } from "semantic-ui-react";

const LoaderMessageComponentElement = () => {
  return (
    <div>
      <Loader active inline="centered" size="medium">
        Se încarcă datele...
      </Loader>
    </div>
  );
};

export default LoaderMessageComponentElement;
