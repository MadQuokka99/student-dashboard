import React, { useEffect, useState, useCallback } from "react";
import { Form } from "semantic-ui-react";
import { getFacultateColor } from "../../Utils/GetStyleByID_Departament";
import { getKeysOfObject } from "../../Utils/Functions";

const optiuniSubgrupa = [
  {
    key: 1,
    text: "Subgrupa A",
    value: "A",
  },
  {
    key: 2,
    text: "Subgrupa B",
    value: "B",
  },
];

const SetariFacultate = (props) => {
  const [facultateaAleasa, setFacultateaAleasa] = useState(
    props.optiuniFacultate[0]
  );
  const [subgrupaAleasa, setSubgrupaAleasa] = useState("");
  const [setariAvatar, setSetariAvatar] = useState({});

  const fetchData = useCallback(() => {
    const facultate = props.optiuniFacultate.filter(
      (fac) => fac.info.ID_Student === props.setari.ID_Student
    )[0];
    setFacultateaAleasa(facultate);

    if (props.setari !== undefined) {
      if (props.setari.Subgrupa !== "") {
        const subgrupa = optiuniSubgrupa.filter(
          (subgrupa) => subgrupa.value === props.setari.Subgrupa
        )[0];
        setSubgrupaAleasa(subgrupa);
      }
      setSetariAvatar(props.setari.SetariAvatar);
    }
  }, [props.optiuniFacultate, props.setari]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const functieCreareAvatar = useCallback(
    (sexUser, numeUser, culoareBackground) => {
      let objSetariAvatar = {
        top: setariAvatar.HairStyle,
        topChance: setariAvatar.HairStyleProbability,
        hairColor: setariAvatar.HairColor,
        accessories: setariAvatar.Accessories,
        accessoriesChance: setariAvatar.AccessoriesProbability,
        facialHair: setariAvatar.FacialHair,
        facialHairChance: setariAvatar.FacialHairProbability,
        facialHairColor: setariAvatar.FacialHairColor,
        clothes: setariAvatar.Clothes,
        clothesColor: setariAvatar.ClothesColor,
        eyes: setariAvatar.Eyes,
        eyebrow: setariAvatar.EyeBrow,
        mouth: setariAvatar.Mouth,
        skin: setariAvatar.Skin,
      };

      let baseURL = `https://avatars.dicebear.com/api/avataaars/${sexUser}/:${numeUser}.svg?backgroundColor=${culoareBackground}&scale=100`;

      const listaCampuriObjSetari = getKeysOfObject(objSetariAvatar);
      let profil = baseURL;

      listaCampuriObjSetari.forEach((element) => {
        if (objSetariAvatar[element] !== null) {
          if (
            typeof objSetariAvatar[element] === "string" ||
            objSetariAvatar[element] instanceof String
          ) {
            profil = profil.concat(
              `&${element}[]=${objSetariAvatar[element][0]}`
            );
          } else {
            profil = profil.concat(`&${element}=${objSetariAvatar[element]}`);
          }
        }
      });

      return profil;
    },
    [setariAvatar]
  );

  const handleFacultateAleasa = (e, { value }) => {
    const facultate = props.optiuniFacultate.filter(
      (fac) => fac.value === value
    )[0];
    setFacultateaAleasa(facultate);

    const culoareFacultate = getFacultateColor(facultate.info.ID_Departament);
    const culoare = culoareFacultate.substr(1, 6);
    const culoareBackground = "%23" + culoare;

    const setariCopy = { ...props.setari };
    setariCopy.ID_Student = facultate.info.ID_Student;
    setariCopy.ID_Facultate = facultate.info.ID_Facultate;
    setariCopy.ID_Departament = facultate.info.ID_Departament;
    setariCopy.SetariAvatar.BackgroundColor = culoareBackground;

    let sexUser = facultate.info.Sex === "F" ? "female" : "male";
    let numeUser = facultate.info.NumeIntreg.trim();

    const linkAvatar = functieCreareAvatar(
      sexUser,
      numeUser,
      culoareBackground
    );
    setariCopy.SetariAvatar.LinkAvatar = linkAvatar;

    props.updateSetari(setariCopy);
  };

  const handleSubgrupaAleasa = (e, { value }) => {
    const subgrupa = optiuniSubgrupa.filter(
      (subgrupa) => subgrupa.value === value
    )[0];
    setSubgrupaAleasa(subgrupa);
    const setariCopy = { ...props.setari };
    setariCopy.Subgrupa = subgrupa.value;
    props.updateSetari(setariCopy);
  };

  return (
    <>
      <p style={{ textAlign: "center" }}>
        Facultatea și specializarea alease sunt:
      </p>
      <p style={{ textAlign: "center" }}>
        <strong>{props.facultateAleasa.text}</strong>
      </p>

      {props.optiuniFacultate.length > 1 && (
        <>
          <Form style={{ display: "flex", justifyContent: "center" }}>
            <Form.Dropdown
              options={props.optiuniFacultate}
              onChange={handleFacultateAleasa}
              selection
              value={facultateaAleasa.value}
              label="Alege facultatea preferată"
            />
          </Form>

          <br />
        </>
      )}

      <br />
      <p style={{ textAlign: "center" }}>
        Subgrupa aleasă este: <strong> subgrupa {props.setari.Subgrupa}</strong>
      </p>
      <Form
        style={{
          display: "flex",
          justifyContent: "center",
          textAlign: "center",
        }}
      >
        <Form.Dropdown
          options={optiuniSubgrupa}
          onChange={handleSubgrupaAleasa}
          selection
          value={subgrupaAleasa.value}
          label="Alege subgrupa preferată"
        />
      </Form>
    </>
  );
};

export default SetariFacultate;
