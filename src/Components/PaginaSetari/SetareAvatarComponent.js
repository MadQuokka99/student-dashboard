import React from "react";
import { Form, Grid } from "semantic-ui-react";

const SetareAvatarComponent = (props) => {
  return (
    <Grid.Row columns={2}>
      <Grid.Column>{props.denumireCamp}</Grid.Column>
      <Grid.Column>
        <Form>
          {props.tipCamp === "dropdown" ? (
            <Form.Dropdown
              selection
              name={props.denumireDropdown}
              options={props.options}
              value={props.value}
              onChange={props.updateValue}
              disabled={props.isDisabled}
            />
          ) : (
            <Form.Input
              type="range"
              name={props.denumireValueRangePicker}
              min={0}
              max={100}
              label={`Valoare: ${props.value}`}
              onChange={props.updateValueRangePicker}
              step={10}
              value={props.value}
            />
          )}
        </Form>
      </Grid.Column>
    </Grid.Row>
  );
};

export default SetareAvatarComponent;
