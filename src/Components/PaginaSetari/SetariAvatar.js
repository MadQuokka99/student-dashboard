import React, { useCallback, useEffect, useState } from "react";
import { Grid, Button, Image } from "semantic-ui-react";
import SetareAvatarComponent from "./SetareAvatarComponent";
import {
  HairStyle,
  HairColor,
  Accessories,
  FacialHair,
  Clothes,
  ClothesColor,
  Eyes,
  EyesBrow,
  Mouth,
  Skin,
} from "../../Utils/SetariAvatar";
import { getKeysOfObject } from "../../Utils/Functions";

const SetariAvatar = (props) => {
  //state pentru setarile cu dropdown
  const [hairStyle, setHairStyle] = useState("none");
  const [hairColor, setHairColor] = useState("none");
  const [accessories, setAccessories] = useState("none");
  const [facialHair, setFacialHair] = useState("none");
  const [facialHairColor, setFacialHairColor] = useState("none");
  const [clothes, setClothes] = useState("none");
  const [clothesColor, setClothesColor] = useState("none");
  const [eyes, setEyes] = useState("none");
  const [eyeBrow, setEyeBrow] = useState("none");
  const [mouth, setMouth] = useState("none");
  const [skin, setSkin] = useState("none");

  //setari pentru setari cu range
  const [topChance, setTopChance] = useState(0);
  const [accessoriesChance, setAccessoriesChance] = useState(0);
  const [facialHairChance, setFacialHairChance] = useState(0);

  //state pentru link-ul de la avatar
  const [avatarStudent, setAvatarStudent] = useState("");
  const [sexUser, setSexUser] = useState("female");
  const [backgroundColor, setBackgroundColor] = useState("%23ffffff");
  const [numeUser, setNumeUser] = useState("John");

  const [isModified, setIsModified] = useState(false);

  const functieCreareAvatar = useCallback(
    (denumire, valoare) => {
      let objSetariAvatar = {
        top: hairStyle,
        topChance: topChance,
        hairColor: hairColor,
        accessories: accessories,
        accessoriesChance: accessoriesChance,
        facialHair: facialHair,
        facialHairChance: facialHairChance,
        facialHairColor: facialHairColor,
        clothes: clothes,
        clothesColor: clothesColor,
        eyes: eyes,
        eyebrow: eyeBrow,
        mouth: mouth,
        skin: skin,
      };

      objSetariAvatar[denumire] = valoare;

      let baseURL = `https://avatars.dicebear.com/api/avataaars/${sexUser}/:${numeUser}.svg?backgroundColor=${backgroundColor}&scale=100`;

      const listaCampuriObjSetari = getKeysOfObject(objSetariAvatar);
      let profil = baseURL;

      listaCampuriObjSetari.forEach((element) => {
        if (objSetariAvatar[element] !== "none") {
          if (
            typeof objSetariAvatar[element] === "string" ||
            objSetariAvatar[element] instanceof String
          ) {
            profil = profil.concat(`&${element}[]=${objSetariAvatar[element]}`);
          } else {
            profil = profil.concat(`&${element}=${objSetariAvatar[element]}`);
          }
        }
      });

      setAvatarStudent(profil);
    },
    [
      numeUser,
      sexUser,
      backgroundColor,
      accessories,
      accessoriesChance,
      clothes,
      clothesColor,
      eyes,
      eyeBrow,
      facialHair,
      facialHairChance,
      facialHairColor,
      hairColor,
      hairStyle,
      mouth,
      skin,
      topChance,
    ]
  );

  const setValueForDropdownFromSettings = useCallback(
    (denumireSetare) => {
      let valueSettingsFromBD = null;
      let valueArray = [
        {
          key: 1,
          text: "none",
          value: "none",
        },
      ];

      if (props.setari !== null) {
        if (denumireSetare === "HairStyle") {
          valueSettingsFromBD = props.setari.SetariAvatar.HairStyle;
          valueArray = HairStyle;
        } else if (denumireSetare === "HairColor") {
          valueSettingsFromBD = props.setari.SetariAvatar.HairColor;
          valueArray = HairColor;
        } else if (denumireSetare === "Accessories") {
          valueSettingsFromBD = props.setari.SetariAvatar.Accessories;
          valueArray = Accessories;
        } else if (denumireSetare === "FacialHair") {
          valueSettingsFromBD = props.setari.SetariAvatar.FacialHair;
          valueArray = FacialHair;
        } else if (denumireSetare === "FacialHairColor") {
          valueSettingsFromBD = props.setari.SetariAvatar.FacialHairColor;
          valueArray = HairColor;
        } else if (denumireSetare === "Clothes") {
          valueSettingsFromBD = props.setari.SetariAvatar.Clothes;
          valueArray = Clothes;
        } else if (denumireSetare === "ClothesColor") {
          valueSettingsFromBD = props.setari.SetariAvatar.ClothesColor;
          valueArray = ClothesColor;
        } else if (denumireSetare === "Eyes") {
          valueSettingsFromBD = props.setari.SetariAvatar.Eyes;
          valueArray = Eyes;
        } else if (denumireSetare === "EyeBrow") {
          valueSettingsFromBD = props.setari.SetariAvatar.EyeBrow;
          valueArray = EyesBrow;
        } else if (denumireSetare === "Mouth") {
          valueSettingsFromBD = props.setari.SetariAvatar.Mouth;
          valueArray = Mouth;
        } else if (denumireSetare === "Skin") {
          valueSettingsFromBD = props.setari.SetariAvatar.Skin;
          valueArray = Skin;
        }
      }

      let valueFilter = null;
      if (
        props.setari === null ||
        valueSettingsFromBD === null ||
        valueSettingsFromBD.length === 0
      ) {
        valueFilter = valueArray.filter(
          (itemValue) => itemValue.value === "none"
        )[0].value;
      } else {
        valueFilter = valueArray.filter(
          (itemValue) => itemValue.value === valueSettingsFromBD[0]
        )[0].value;
      }

      if (denumireSetare === "HairStyle") {
        setHairStyle(valueFilter);
      } else if (denumireSetare === "HairColor") {
        setHairColor(valueFilter);
      } else if (denumireSetare === "Accessories") {
        setAccessories(valueFilter);
      } else if (denumireSetare === "FacialHair") {
        setFacialHair(valueFilter);
      } else if (denumireSetare === "FacialHairColor") {
        setFacialHairColor(valueFilter);
      } else if (denumireSetare === "Clothes") {
        setClothes(valueFilter);
      } else if (denumireSetare === "ClothesColor") {
        setClothesColor(valueFilter);
      } else if (denumireSetare === "Eyes") {
        setEyes(valueFilter);
      } else if (denumireSetare === "EyeBrow") {
        setEyeBrow(valueFilter);
      } else if (denumireSetare === "Mouth") {
        setMouth(valueFilter);
      } else if (denumireSetare === "Skin") {
        setSkin(valueFilter);
      }
    },
    [props.setari]
  );

  const setValueForRangePickerFromSettings = useCallback(
    (denumireSetare) => {
      let valueSettingsFromBD = null;

      if (props.setari !== null) {
        if (denumireSetare === "TopChance") {
          valueSettingsFromBD = props.setari.SetariAvatar.HairStyleProbability;
          setTopChance(valueSettingsFromBD);
        } else if (denumireSetare === "AccessoriesChance") {
          valueSettingsFromBD =
            props.setari.SetariAvatar.AccessoriesProbability;
          setAccessoriesChance(valueSettingsFromBD);
        } else if (denumireSetare === "FacialHairChance") {
          valueSettingsFromBD = props.setari.SetariAvatar.FacialHairProbability;
          setFacialHairChance(valueSettingsFromBD);
        }
      }
    },
    [props.setari]
  );

  const handleSetInitialValuesForSettingsAvatar = useCallback(() => {
    if (props.setari !== null) {
      setAvatarStudent(props.setari.SetariAvatar.LinkAvatar);
    }

    setValueForDropdownFromSettings("HairStyle");
    setValueForDropdownFromSettings("HairColor");
    setValueForDropdownFromSettings("Accessories");
    setValueForDropdownFromSettings("FacialHair");
    setValueForDropdownFromSettings("FacialHairColor");
    setValueForDropdownFromSettings("Clothes");
    setValueForDropdownFromSettings("ClothesColor");
    setValueForDropdownFromSettings("Eyes");
    setValueForDropdownFromSettings("EyeBrow");
    setValueForDropdownFromSettings("Mouth");
    setValueForDropdownFromSettings("Skin");

    setValueForRangePickerFromSettings("TopChance");
    setValueForRangePickerFromSettings("AccessoriesChance");
    setValueForRangePickerFromSettings("FacialHairChance");
  }, [
    props.setari,
    setValueForDropdownFromSettings,
    setValueForRangePickerFromSettings,
  ]);

  useEffect(() => {
    if (props.facultateAleasa !== null) {
      setNumeUser(props.facultateAleasa.info.NumeIntreg.trim());
    }
    if (props.setari !== null) {
      setSexUser(props.setari.SetariAvatar.Sex);
      setBackgroundColor(props.setari.SetariAvatar.BackgroundColor);
    }
    handleSetInitialValuesForSettingsAvatar();
  }, [
    props.setari,
    props.facultateAleasa,
    handleSetInitialValuesForSettingsAvatar,
  ]);

  const handleSalvareSetariAvatar = (e) => {
    const setariCopy = { ...props.setari };

    let hairStyleValue = hairStyle;
    let hairColorValue = hairColor;
    let accessoriesValue = accessories;
    let facialHairValue = facialHair;
    let facialHairColorValue = facialHairColor;
    let clothesValue = clothes;
    let clothesColorValue = clothesColor;
    let eyesValue = eyes;
    let eyeBrowValue = eyeBrow;
    let mouthValue = mouth;
    let skinValue = skin;

    hairStyle === "none"
      ? (hairStyleValue = null)
      : (hairStyleValue = [hairStyleValue]);
    hairColor === "none"
      ? (hairColorValue = null)
      : (hairColorValue = [hairColorValue]);
    accessories === "none"
      ? (accessoriesValue = null)
      : (accessoriesValue = [accessoriesValue]);
    facialHair === "none"
      ? (facialHairValue = null)
      : (facialHairValue = [facialHairValue]);
    facialHairColor === "none"
      ? (facialHairColorValue = null)
      : (facialHairColorValue = [facialHairColorValue]);
    clothes === "none"
      ? (clothesValue = null)
      : (clothesValue = [clothesValue]);
    clothesColor === "none"
      ? (clothesColorValue = null)
      : (clothesColorValue = [clothesColorValue]);
    eyes === "none" ? (eyesValue = null) : (eyesValue = [eyesValue]);
    eyeBrow === "none"
      ? (eyeBrowValue = null)
      : (eyeBrowValue = [eyeBrowValue]);
    mouth === "none" ? (mouthValue = null) : (mouthValue = [mouthValue]);
    skin === "none" ? (skinValue = null) : (skinValue = [skinValue]);

    setariCopy.SetariAvatar.HairStyle = hairStyleValue;
    setariCopy.SetariAvatar.HairColor = hairColorValue;
    setariCopy.SetariAvatar.Accessories = accessoriesValue;
    setariCopy.SetariAvatar.FacialHair = facialHairValue;
    setariCopy.SetariAvatar.FacialHairColor = facialHairColorValue;
    setariCopy.SetariAvatar.Clothes = clothesValue;
    setariCopy.SetariAvatar.ClothesColor = clothesColorValue;
    setariCopy.SetariAvatar.Eyes = eyesValue;
    setariCopy.SetariAvatar.EyeBrow = eyeBrowValue;
    setariCopy.SetariAvatar.Mouth = mouthValue;
    setariCopy.SetariAvatar.Skin = skinValue;
    setariCopy.SetariAvatar.HairStyleProbability = parseInt(topChance);
    setariCopy.SetariAvatar.AccessoriesProbability =
      parseInt(accessoriesChance);
    setariCopy.SetariAvatar.FacialHairProbability = parseInt(facialHairChance);
    setariCopy.SetariAvatar.LinkAvatar = avatarStudent;

    props.updateSetari(setariCopy);
    setIsModified(false);
  };

  const handleValueRangePicker = (e, { name, value }) => {
    if (name === "topChance") {
      setTopChance(value);
    } else if (name === "accessoriesChance") {
      setAccessoriesChance(value);
    } else if (name === "facialHairChance") {
      setFacialHairChance(value);
    }

    functieCreareAvatar(name, value);
    setIsModified(true);
  };

  const handleDropdownValue = (e, { name, value }) => {
    let filter = "none";
    if (name === "top") {
      filter = HairStyle.filter((itemValue) => itemValue.value === value)[0]
        .value;
      setHairStyle(filter);
    } else if (name === "hairColor") {
      filter = HairColor.filter((itemValue) => itemValue.value === value)[0]
        .value;
      setHairColor(filter);
    } else if (name === "accessories") {
      filter = Accessories.filter((itemValue) => itemValue.value === value)[0]
        .value;
      setAccessories(filter);
    } else if (name === "facialHair") {
      filter = FacialHair.filter((itemValue) => itemValue.value === value)[0]
        .value;
      setFacialHair(filter);
    } else if (name === "facialHairColor") {
      filter = HairColor.filter((itemValue) => itemValue.value === value)[0]
        .value;
      setFacialHairColor(filter);
    } else if (name === "clothes") {
      filter = Clothes.filter((itemValue) => itemValue.value === value)[0]
        .value;
      setClothes(filter);
    } else if (name === "clothesColor") {
      filter = ClothesColor.filter((itemValue) => itemValue.value === value)[0]
        .value;
      setClothesColor(filter);
    } else if (name === "eyes") {
      filter = Eyes.filter((itemValue) => itemValue.value === value)[0].value;
      setEyes(filter);
    } else if (name === "eyebrow") {
      filter = EyesBrow.filter((itemValue) => itemValue.value === value)[0]
        .value;
      setEyeBrow(filter);
    } else if (name === "mouth") {
      filter = Mouth.filter((itemValue) => itemValue.value === value)[0].value;
      setMouth(filter);
    } else if (name === "skin") {
      filter = Skin.filter((itemValue) => itemValue.value === value)[0].value;
      setSkin(filter);
    }

    functieCreareAvatar(name, filter);
    setIsModified(true);
  };

  const handleResetSetariAvatar = (e) => {
    handleSetInitialValuesForSettingsAvatar();
    setIsModified(false);
  };

  return (
    <Grid
      textAlign="left"
      stackable
      style={{ maxWidth: "1700px", padding: "0 10%" }}
    >
      <Grid.Row columns={2} textAlign="center">
        <Grid.Column verticalAlign="middle" style={{paddingBottom: "1em"}}>
          <Button
            onClick={handleSalvareSetariAvatar}
            compact
            type="button"
            disabled={!isModified}
          >
            ✅ Salvează
          </Button>
          <Button
            onClick={handleResetSetariAvatar}
            compact
            type="button"
            disabled={!isModified}
          >
            ❌ Anulează
          </Button>
        </Grid.Column>
        <Grid.Column style={{ display: "flex", justifyContent: "center" }}>
          <Image src={avatarStudent} size="tiny" />
        </Grid.Column>
      </Grid.Row>
      <SetareAvatarComponent
        denumireCamp="Probabilitate păr"
        value={topChance}
        updateValueRangePicker={handleValueRangePicker}
        denumireValueRangePicker="topChance"
      />

      <SetareAvatarComponent
        denumireCamp="Stil păr"
        tipCamp="dropdown"
        options={HairStyle}
        value={hairStyle}
        denumireDropdown={"top"}
        updateValue={handleDropdownValue}
        isDisabled={topChance < 50}
      />

      <SetareAvatarComponent
        denumireCamp="Culoare păr"
        tipCamp="dropdown"
        options={HairColor}
        value={hairColor}
        denumireDropdown={"hairColor"}
        updateValue={handleDropdownValue}
      />
      <SetareAvatarComponent
        denumireCamp="Probabilitate accesorii"
        value={accessoriesChance}
        updateValueRangePicker={handleValueRangePicker}
        denumireValueRangePicker="accessoriesChance"
      />
      <SetareAvatarComponent
        denumireCamp="Accesorii"
        tipCamp="dropdown"
        options={Accessories}
        value={accessories}
        updateValue={handleDropdownValue}
        denumireDropdown={"accessories"}
        isDisabled={accessoriesChance < 70}
      />
      <SetareAvatarComponent
        denumireCamp="Probabilitate păr facial"
        value={facialHairChance}
        updateValueRangePicker={handleValueRangePicker}
        denumireValueRangePicker="facialHairChance"
      />
      <SetareAvatarComponent
        denumireCamp="Păr facial"
        tipCamp="dropdown"
        options={FacialHair}
        value={facialHair}
        updateValue={handleDropdownValue}
        denumireDropdown={"facialHair"}
        isDisabled={facialHairChance < 60}
      />
      <SetareAvatarComponent
        denumireCamp="Culoare păr facial"
        tipCamp="dropdown"
        options={HairColor}
        value={facialHairColor}
        updateValue={handleDropdownValue}
        denumireDropdown={"facialHairColor"}
        isDisabled={facialHairChance < 60}
      />
      <SetareAvatarComponent
        denumireCamp="Haine"
        tipCamp="dropdown"
        options={Clothes}
        value={clothes}
        updateValue={handleDropdownValue}
        denumireDropdown={"clothes"}
      />
      <SetareAvatarComponent
        denumireCamp="Culoare haine"
        tipCamp="dropdown"
        options={ClothesColor}
        value={clothesColor}
        updateValue={handleDropdownValue}
        denumireDropdown={"clothesColor"}
      />
      <SetareAvatarComponent
        denumireCamp="Ochii"
        tipCamp="dropdown"
        options={Eyes}
        value={eyes}
        updateValue={handleDropdownValue}
        denumireDropdown={"eyes"}
      />
      <SetareAvatarComponent
        denumireCamp="Sprâncene"
        tipCamp="dropdown"
        options={EyesBrow}
        value={eyeBrow}
        updateValue={handleDropdownValue}
        denumireDropdown={"eyebrow"}
      />
      <SetareAvatarComponent
        denumireCamp="Gură"
        tipCamp="dropdown"
        options={Mouth}
        value={mouth}
        updateValue={handleDropdownValue}
        denumireDropdown={"mouth"}
      />
      <SetareAvatarComponent
        denumireCamp="Culoarea pielii"
        tipCamp="dropdown"
        options={Skin}
        value={skin}
        updateValue={handleDropdownValue}
        denumireDropdown={"skin"}
      />
    </Grid>
  );
};

export default SetariAvatar;
