import React, { useEffect, useState } from "react";
import { Form } from "semantic-ui-react";

const optiuniCantina = [
  {
    key: 1,
    text: "Cantină Memo",
    value: "memo",
  },
  {
    key: 2,
    text: "Cantină Colină",
    value: "colina",
  },
];

const SetariCantina = (props) => {
  const [cantinaPreferata, setCantinaPreferata] = useState(optiuniCantina[0]);

  useEffect(() => {
    const cantinaPreferataObj = optiuniCantina.filter(
      (cantina) => cantina.value === props.setari.CantinaPreferata
    )[0];
    setCantinaPreferata(cantinaPreferataObj);
  }, [props.setari.CantinaPreferata]);

  const handleCantinaAleasa = (e, { value }) => {
    const setariCopy = { ...props.setari };
    setariCopy.CantinaPreferata = value;
    props.updateSetari(setariCopy);
  };
  return (
    <>
      <p style={{textAlign: "center"}}>
        Cantina aleasă este: <strong>{cantinaPreferata.text}</strong>
      </p>
      <br/>
      <Form style={{ display: "flex", justifyContent: "center" }}>
        <Form.Dropdown
          options={optiuniCantina}
          onChange={handleCantinaAleasa}
          selection
          value={cantinaPreferata.value}
          label="Alege cantina preferată"
        />
      </Form>
    </>
  );
};

export default SetariCantina;
