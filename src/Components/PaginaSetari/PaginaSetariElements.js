import styled from "styled-components";
import { keyframes } from "styled-components";
import { Grid } from "semantic-ui-react";

const slideInAnimation = keyframes`
    0% { margin-left:-900px; }
    20% { margin-left:-900px; }
    35% { margin-left:-16px; }
    100% { margin-left:-16px; }
`;

export const GridWithTransition = styled(Grid)`
  && {
    animation-name: ${slideInAnimation};
    animation-duration: 3s;
  }
`;
