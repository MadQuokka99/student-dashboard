import React, { useEffect, useState } from "react";
import { Segment, Grid, Header } from "semantic-ui-react";

const ItemBurseComponent = (props) => {
  const [itemValue, setItemValue] = useState({});

  useEffect(() => {
    if (props.detaliiItem !== null) {
      setItemValue(props.detaliiItem);
    }
  }, [props.detaliiItem]);

  return (
    <Segment style={{ borderRadius: "6px" }} textAlign={"left"}>
      <Grid stackable>
        <Grid.Row centered>
          <Grid.Column verticalAlign={"middle"}>
            <Header as="h5">{`Bursă ${itemValue.DenumireTipBursa}`}</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={2}>
          <Grid.Column>
            {`📆 Perioadă: ${itemValue.DenumireTipPerioadaAcordareBursa}`}
          </Grid.Column>
          <Grid.Column>
            {`📚 Specializare: ${itemValue.DenumireSpecializare}`}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};

export default ItemBurseComponent;
