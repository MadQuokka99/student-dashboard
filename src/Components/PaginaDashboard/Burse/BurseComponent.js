import React, { useCallback, useEffect, useState } from "react";
import { bursaListByStudentAnUniv } from "../../../Utils/ApiCalls";
import { Message } from "semantic-ui-react";
import ErrorMessageComponent from "../../GeneralElements/ErrorMessageComponent";
import LoaderMessageComponentElement from "../../GeneralElements/LoaderMessageComponentElement";
import ItemBurseComponent from "./ItemBurseComponent";
import { v4 as uuidv4 } from "uuid";
import { getUsername } from "../../../Utils/GetUsername";

const BurseComponent = (props) => {
  const [burseStudent, setBurseStudent] = useState([]);
  const [error, setError] = useState(null); //folosit pentru afisarea mesajului de eroare daca nu vin datele de afisat
  const [isLoading, setIsLoading] = useState(false);

  const username = getUsername();

  const fetchData = useCallback(
    async (ID_AnUniv, ID_Student) => {
      setIsLoading(true);
      setError(null);

      try {
        const burseResponse = await bursaListByStudentAnUniv(
          username,
          ID_AnUniv
        );

        let burseList = [];
        for (let i = 0; i < burseResponse.data.length; i++) {
          if (burseResponse.data[i].ID_Student === ID_Student) {
            burseList.push(burseResponse.data[i]);
          }
        }

        setBurseStudent(burseList);
      } catch (error) {
        setError(error.response);
      }
      setIsLoading(false);
    },
    [username]
  );

  useEffect(() => {
    if (props.setari !== null) {
      fetchData(props.setari.ID_AnUniv, props.setari.ID_Student);
    }
  }, [fetchData, props.setari]);

  let content = null;

  content =
    burseStudent.length === 0 ? (
      <Message info content="😕 Nu există informații de afișat!" />
    ) : (
      <>
        {burseStudent.map((item) => (
          <ItemBurseComponent detaliiItem={item} key={uuidv4()} />
        ))}
      </>
    );

  if (error) {
    content = <ErrorMessageComponent error={error} />;
  }

  if (isLoading) {
    content = <LoaderMessageComponentElement />;
  }

  return content;
};

export default BurseComponent;
