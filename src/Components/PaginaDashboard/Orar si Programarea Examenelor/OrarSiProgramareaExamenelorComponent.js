import React, { useCallback, useEffect, useState } from "react";
import {
  getOrarByStudent,
  programareExamenListByStudentAnUniv,
} from "../../../Utils/ApiCalls";

import LoaderMessageComponentElement from "../../GeneralElements/LoaderMessageComponentElement";
import ErrorMessageComponent from "../../GeneralElements/ErrorMessageComponent";
import { SegmentComponent } from "../DashboardPageElements";

import { Message } from "semantic-ui-react";
import moment from "moment";

import FullCalendar from "@fullcalendar/react"; // must go before plugins
import dayGridPlugin from "@fullcalendar/daygrid"; // a plugin!
import roLocale from "@fullcalendar/core/locales/ro";
import listPlugin from "@fullcalendar/list";

const OrarSiProgramareaExamenelorComponent = (props) => {
  const [examene, setExamene] = useState([]);
  const [error, setError] = useState(null); //folosit pentru afisarea mesajului de eroare daca nu vin datele de afisat
  const [isLoading, setIsLoading] = useState(false);
  const [tipSaptamana, setTipSaptamana] = useState(-1);

  const fetchData = useCallback(async (setari, studentInfo) => {
    setIsLoading(true);
    setError(null);
    try {
      const exameneResponse = await programareExamenListByStudentAnUniv(
        setari.ID_Student,
        setari.ID_AnUniv
      );

      let listaExamene = [];

      exameneResponse.data.forEach((element) => {
        let item = {
          title: element.Denumire + " | " + element.Sala,
          start: element.Data,
          color: "#ff7070",
        };

        // sunt afisate doar examenele care sunt permise a fi vizualizate de catre secretariat
        if(element.ActivaStudenti) {
            listaExamene.push(item);
        }
      });

      const grupa = studentInfo.DenumireGrupa;
      const orarResponse = await getOrarByStudent(
        "",
        "",
        grupa,
        setari.Subgrupa
      );

      orarResponse.data.forEach((element) => {
        let item = {
          title:
            element.activitate +
            " " +
            element.materie +
            " | " +
            element.sala +
            " | " +
            element.profesor,
          start: moment(element.data, "DD.MM.YYYY")
            .format("YYYY-MM-DD")
            .concat("T", element.start),
          end: moment(element.data, "DD.MM.YYYY")
            .format("YYYY-MM-DD")
            .concat("T", element.end),
          color:
            element.activitate === "Curs"
              ? "#4DB74F"
              : element.activitate === "Seminar"
              ? "#00B3CC"
              : element.activitate === "Laborator"
              ? "#7B44E9"
              : element.activitate === "Proiect"
              ? "#DA1A63"
              : "#1E70BF",
        };
        listaExamene.push(item);

        if (element.data === moment().format("DD.MM.YYYY")) {
          //daca e saptamana para e 1, daca e impara e 0
          setTipSaptamana(element.saptamana % 2 === 0 ? 1 : 0);
        }
      });

      setExamene(listaExamene);
    } catch (error) {
      setError(error.response);
    }
    setIsLoading(false);
  }, []);

  useEffect(() => {
    if (props.setari !== undefined && props.studentInfo !== undefined) {
      if (props.setari !== null && props.studentInfo !== null) {
        fetchData(props.setari, props.studentInfo);
      }
    }
  }, [fetchData, props.setari, props.studentInfo]);

  const trimiteCatrePaginaCursuri = (e) => {
    const url = "https://intranet.unitbv.ro/secretariat/cursurilemele";
    window.open(url, "_black");
  };

  let content = null;
  content =
    examene.length === 0 ? (
      <Message info content="😕 Nu există examene de afișat!" />
    ) : (
      <>
        {tipSaptamana !== -1 && (
          <SegmentComponent
            onClick={trimiteCatrePaginaCursuri}
            style={{ textAlign: "center" }}
          >
            {`Săptămână ${
              tipSaptamana === 1 ? "pară" : tipSaptamana === 0 ? "impară" : "-"
            }`}
          </SegmentComponent>
        )}
        <FullCalendar
          plugins={[listPlugin, dayGridPlugin]}
          initialView={"listDay"}
          contentHeight={250}
          navLinks={true}
          locale={roLocale} //todo: actualizare in functie de alegerile userului
          events={examene}
          fixedWeekCount={false}
          selectable={true}
          headerToolbar={
            props.screenWidth < 600
              ? {
                  left: "prev,next",
                  center: "title",
                  right: "today,listWeek,dayGridMonth",
                }
              : {
                  left: "prev,next",
                  center: "title",
                  right: "today,listDay,listWeek,dayGridMonth",
                }
          }
          //todo: de actualizat denumirea butoanelor in functie de alegerile userului
          views={{
            listDay: {
              buttonText: "Zi",
              titleFormat: {
                year: "numeric",
                month: "2-digit",
                day: "2-digit",
              },
            },
            listWeek: {
              buttonText: props.screenWidth < 1200 ? "Săpt" : "Săptămână",
              titleFormat:
                props.screenWidth < 800
                  ? { year: "numeric", month: "2-digit", day: "2-digit" }
                  : { year: "numeric", month: "long", day: "numeric" },
            },
            listMonth: {
              buttonText: "Lună",
              titleFormat:
                props.screenWidth < 800
                  ? { year: "numeric", month: "2-digit", day: "2-digit" }
                  : { year: "numeric", month: "long", day: "numeric" },
            },
          }}
        />
      </>
    );

  if (error) {
    content = <ErrorMessageComponent error={error} />;
  }

  if (isLoading) {
    content = <LoaderMessageComponentElement />;
  }

  return content;
};

export default OrarSiProgramareaExamenelorComponent;
