import React, { useEffect, useState } from "react";
import { Button } from "semantic-ui-react";

const ItemTurVirtualComponent = (props) => {
  const [titlu, setTitlu] = useState("");
  const [link, setLink] = useState("");

  useEffect(() => {
    if (props.titlu !== null) {
      setTitlu(props.titlu);
    }

    if (props.link !== null) {
      setLink(props.link);
    }
  }, [props.titlu, props.link]);

  const handleClick = (e) => {
    window.open(link, "_black");
  };

  return (
    <>
      <Button
        compact
        onClick={handleClick}
        fluid
        type="button"
        style={{ height: props.screenWidth < 600 && "6.5vh" }}
      >
        {titlu}
      </Button>
    </>
  );
};

export default ItemTurVirtualComponent;
