import React from "react";
import { Grid } from "semantic-ui-react";
import ItemTurVirtualComponent from "./ItemTurVirtualComponent";

const TurVirtualComponent = () => {
  return (
    <Grid columns={3} doubling>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🏠 A"} link={"https://www.unitbv.ro/tur-virtual/corp-a.html"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🏠 B"} link={"https://www.unitbv.ro/tur-virtual/corp-b.html"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🏠 C"} link={"https://www.unitbv.ro/tur-virtual/corp-c.html"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🏠 D"} link={"https://www.unitbv.ro/tur-virtual/corp-d.html"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🏠 F și I"} link={"https://www.unitbv.ro/tur-virtual/corpurile-f-si-i.html"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🏠 H"} link={"https://www.unitbv.ro/tur-virtual/corp-h.html"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🏠 J"} link={"#"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🏠 K"} link={"#"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🏠 L"} link={"https://www.unitbv.ro/tur-virtual/corp-l.html"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🏠 Q"} link={"https://www.unitbv.ro/tur-virtual/corp-q.html"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🏠 R"} link={"#"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🏠 S"} link={"#"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🏠 T"} link={"#"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🏠 W"} link={"https://www.unitbv.ro/tur-virtual/corp-w.html"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"⛰ Colina"} link={"https://www.unitbv.ro/tur-virtual/exterior-colina-universitatii.html"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🥣 Cantina Colină"} link={"https://www.unitbv.ro/tur-virtual/cantina-colina-universitatii.html"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"🎓 Aula"} link={"#"} />
      </Grid.Column>
      <Grid.Column>
        <ItemTurVirtualComponent titlu={"📚 Biblioteca"} link={"#"} />
      </Grid.Column>
    </Grid>
  );
};

export default TurVirtualComponent;
