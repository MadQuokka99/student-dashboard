import React, { useCallback, useEffect, useState } from "react";
import { notificareList } from "../../../Utils/ApiCalls";
import { Message } from "semantic-ui-react";
import ErrorMessageComponent from "../../GeneralElements/ErrorMessageComponent";
import LoaderMessageComponentElement from "../../GeneralElements/LoaderMessageComponentElement";
import ItemNotificariComponent from "./ItemNotificariComponent";

const NotificariComponent = () => {
  const [notificariApp, setNotificariApp] = useState([]);
  const [error, setError] = useState(null); //folosit pentru afisarea mesajului de eroare daca nu vin datele de afisat
  const [isLoading, setIsLoading] = useState(false);

  const fetchData = useCallback(async () => {
    setIsLoading(true);
    setError(null);
    try {
      const notificariResponse = await notificareList();
      const notificari = notificariResponse.data.sort((a,b) => (a.DataAdaugare < b.DataAdaugare) ? 1 : ((b.DataAdaugare < a.DataAdaugare) ? -1 : 0))

      setNotificariApp(notificari);
    } catch (error) {
      setError(error.response);
    }
    setIsLoading(false);
  }, []);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  let content = null;
  content =
    notificariApp.length === 0 ? (
      <Message info content="😕 Nu există notificări de afișat!" />
    ) : (
      <>
        {notificariApp.map((item) => (
          <ItemNotificariComponent
            detaliiItem={item}
            key={item.ID_Notificare}
          />
        ))}
      </>
    );

  if (error) {
    content = <ErrorMessageComponent error={error} />;
  }

  if (isLoading) {
    content = <LoaderMessageComponentElement />;
  }

  return content;
};

export default NotificariComponent;
