import React, { useEffect, useState } from "react";
import { Segment, Header, Grid } from "semantic-ui-react";
import moment from "moment";

const ItemNotificariComponent = (props) => {
  const [itemValue, setItemValue] = useState({});

  useEffect(() => {
    if (props.detaliiItem !== null) {
      setItemValue(props.detaliiItem);
    }
  }, [props.detaliiItem]);

  return (
    <Segment style={{ borderRadius: "6px" }} textAlign={"left"}>
      <Grid>
        <Grid.Row centered>
          <Grid.Column verticalAlign={"middle"}>
            <Header as="h5">{itemValue.TitluNotificare}</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column textAlign="justified">
            {itemValue.TextNotificare}
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            {moment(itemValue.DataAdaugare).format("DD.MM.YYYY h:mm a")}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};

export default ItemNotificariComponent;
