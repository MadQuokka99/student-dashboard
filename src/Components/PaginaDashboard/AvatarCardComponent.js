import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Card, Image, Label, Popup } from "semantic-ui-react";
import Sigla from "../../Utils/Sigla";

const AvatarCardComponent = (props) => {
  const [numeIntregUser, setNumeIntregUser] = useState("");
  const [denumireFacultate, setDenumireFacultate] = useState("");
  const [denumireSpecializare, setDenumireSpecializare] = useState("");
  const [ID_DepartamentFacultateAleasa, setID_DepartamentFacultateAleasa] =
    useState(0);
  const [denumireAnStudiu, setDenumireAnStudiu] = useState("");

  useEffect(() => {
    if (props.facultateaAleasa !== null) {
      setNumeIntregUser(props.facultateaAleasa.info.NumeIntreg);
      setDenumireFacultate(props.facultateaAleasa.info.DenumireFacultate);
      setDenumireSpecializare(props.facultateaAleasa.info.DenumireSpecializare);
      setID_DepartamentFacultateAleasa(
        props.facultateaAleasa.info.ID_Departament
      );
      setDenumireAnStudiu(props.facultateaAleasa.info.DenumireAnStudiu);
    }
  }, [props.facultateaAleasa]);

  return (
    <Card link>
      <Card.Content>
        <div style={{ display: "flex", justifyContent: "right" }}>
          <Link to="/setari">
            <Popup
              trigger={<Label circular size="huge" icon="settings" />}
              position="top right"
              content="Apasă pentru a-ți configura avatarul și preferințele!"
            />
          </Link>
        </div>
        <span style={{ textAlign: "center" }}>
          <Image src={props.avatarSrc} size="tiny" circular />
        </span>
        <Card.Header style={{ marginTop: ".5em", textAlign: "center" }}>
          {numeIntregUser}
        </Card.Header>

        <Card.Meta style={{ textAlign: "center" }}>
          <Sigla icon={ID_DepartamentFacultateAleasa} /> {denumireFacultate}
        </Card.Meta>
        <Card.Description style={{ textAlign: "center" }}>
          {`${denumireSpecializare} - ${denumireAnStudiu}`}
        </Card.Description>
      </Card.Content>
    </Card>
  );
};

export default AvatarCardComponent;
