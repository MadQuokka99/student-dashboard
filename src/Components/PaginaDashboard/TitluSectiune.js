import React, { useEffect, useState } from "react";
import { Label } from "semantic-ui-react";

const TitluSectiune = (props) => {
  const [titlu, setTitlu] = useState("");

  useEffect(() => {
    if (props.titlu !== null) {
      setTitlu(props.titlu);
    }
  }, [props.titlu]);

  return (
    <Label
      style={{
        padding: "0.65em",
        borderRadius: "10px",
        fontSize: ".9rem",
      }}
      attached="top"
    >
      {props.sigla !== undefined && props.sigla}
      {titlu}
    </Label>
  );
};

export default TitluSectiune;
