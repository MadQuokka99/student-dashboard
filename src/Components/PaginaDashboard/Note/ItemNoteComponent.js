import React, { useEffect, useState } from "react";
import { Segment, Label, Grid } from "semantic-ui-react";

const ItemNoteComponent = (props) => {
  const [itemValue, setItemValue] = useState({});
  const [esteNumarNota, setEsteNumarNota] = useState(true);
  const [screenWidth, setScreenWidth] = useState("600px");

  useEffect(() => {
    if (props.detaliiItem !== null) {
      setItemValue(props.detaliiItem);

      if (isNaN(parseInt(props.detaliiItem.Nota))) {
        setEsteNumarNota(false);
      }
    }

    if (props.screenWidthNoteComponent !== null) {
      setScreenWidth(props.screenWidthNoteComponent);
    }
  }, [props.detaliiItem, props.screenWidthNoteComponent]);

  return (
    <Segment style={{ borderRadius: "6px" }} textAlign={"left"}>
      <Grid>
        <Grid.Row columns={2}>
          <Grid.Column
            width={
              screenWidth > 600
                ? esteNumarNota
                  ? 10
                  : 7
                : esteNumarNota
                ? 8
                : 5
            }
            verticalAlign={"middle"}
          >
            {itemValue.Denumire}
          </Grid.Column>
          <Grid.Column width={1} verticalAlign={"middle"}>
            <Label as="a" style={{ marginLeft: "2em" }} size={"medium"}>
              {itemValue.Nota}
            </Label>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};

export default ItemNoteComponent;
