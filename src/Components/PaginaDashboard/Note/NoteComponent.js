import React, { useCallback, useEffect, useState } from "react";
import {
  noteListByID_Student,
  anUniversitar_Get,
} from "../../../Utils/ApiCalls";
import ItemNoteComponent from "./ItemNoteComponent";
import { Message } from "semantic-ui-react";
import ErrorMessageComponent from "../../GeneralElements/ErrorMessageComponent";
import LoaderMessageComponentElement from "../../GeneralElements/LoaderMessageComponentElement";
import moment from "moment";

const NoteComponent = (props) => {
  const [noteStudent, setNoteStudent] = useState([]);
  const [error, setError] = useState(null); //folosit pentru afisarea mesajului de eroare daca nu vin datele de afisat
  const [isLoading, setIsLoading] = useState(false);
  const [nrSemestruDinAn, setNrSemestruDinAn] = useState(1);

  const fetchData = useCallback(
    async (idStudent, idAnUniv) => {
      setIsLoading(true);
      setError(null);
      try {
        const noteResponse = await noteListByID_Student(idStudent);
        const anUniversitar = await anUniversitar_Get(idAnUniv);

        if (
          moment().format("YYYY-MM-DD") >=
            moment(anUniversitar.data.DataInceputSem2).format("YYYY-MM-DD") &&
          moment().month() + 1 < 10
        ) {
          setNrSemestruDinAn(2);
        }

        let noteList = [];

        for (let i = 0; i < noteResponse.data.length; i++) {
          if (
            noteResponse.data[i].ID_AnUniv === idAnUniv &&
            noteResponse.data[i].NumarSemestruDinAn === nrSemestruDinAn
          ) {
            noteList.push(noteResponse.data[i]);
          }
        }

        setNoteStudent(noteList);
      } catch (error) {
        setError(error.response);
      }
      setIsLoading(false);
    },
    [nrSemestruDinAn]
  );

  useEffect(() => {
    if (props.setari !== null) {
      fetchData(props.setari.ID_Student, props.setari.ID_AnUniv);
    }
  }, [fetchData, props.setari]);

  let content = null;
  content =
    noteStudent.length === 0 ? (
      <Message info content="😕 Nu există note de afișat!" />
    ) : (
      <>
        {noteStudent.map((item) => (
          <ItemNoteComponent
            detaliiItem={item}
            key={item.ID_Nota}
            screenWidthNoteComponent={props.screenWidth}
          />
        ))}
      </>
    );

  if (error) {
    content = <ErrorMessageComponent error={error} />;
  }

  if (isLoading) {
    content = <LoaderMessageComponentElement />;
  }

  return content;
};

export default NoteComponent;
