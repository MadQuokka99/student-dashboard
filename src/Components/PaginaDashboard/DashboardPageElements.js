import styled from "styled-components";
import { Segment, Header } from "semantic-ui-react";
import { keyframes } from "styled-components";

const slideInAnimation = keyframes`
    0% { margin-left:-900px; }
    20% { margin-left:-900px; }
    35% { margin-left:0px; }
    100% { margin-left:0px; }
`;

export const SegmentComponent = styled(Segment)`
  && {
    border-radius: 6px;
    max-height: 400px;
    // width: ${(props) =>
      props.width === undefined ? "100% !important" : props.width};
    overflow-y: scroll;

    -ms-overflow-style: none;
    scrollbar-width: none;

    &::-webkit-scrollbar {
      width: 0.5rem;
      display: none;
    }

    &::-webkit-scrollbar-track {
      background: #bcc3c4;
    }

    &::-webkit-scrollbar-thumb {
      background: #a4aaac;
    }

    @media only screen and (min-width: 600px) {
      overflow-y: scroll;
      max-height: ${(props) =>
        props.height === undefined ? "400px" : props.height};
    }
  }
`;

export const IframeTutorialeYoutube = styled.iframe`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
  border: none;
`;

export const DivTutorialeYoutube = styled.div`
  position: relative;
  width: 100%;
  overflow: hidden;
  padding-top: 66.66%; /* 3:2 Aspect Ratio */
`;

export const HeaderComponent = styled(Header)`
  && {
    animation-name: ${slideInAnimation};
    animation-duration: 7s;
    }
  }
`;

export const DivWithTransition = styled.div`
  && {
    // animation-name: ${slideInAnimation};
    // animation-duration: 3s;
  }
`;
