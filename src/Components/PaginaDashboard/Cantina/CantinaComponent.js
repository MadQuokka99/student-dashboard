import React, { useCallback, useEffect, useState } from "react";
import { getMeniuDetailsByDateAndLocation } from "../../../Utils/ApiCalls";
import { Message } from "semantic-ui-react";
import ItemCantinaComponent from "./ItemCantinaComponent";
import ErrorMessageComponent from "../../GeneralElements/ErrorMessageComponent";
import LoaderMessageComponentElement from "../../GeneralElements/LoaderMessageComponentElement";
import moment from "moment";

const CantinaComponent = (props) => {
  const [meniuCantina, setMeniuCantina] = useState([]);
  const [error, setError] = useState(null); //folosit pentru afisarea mesajului de eroare daca nu vin datele de afisat
  const [isLoading, setIsLoading] = useState(false);

  const fetchData = useCallback(async (locatie) => {
    setIsLoading(true);
    setError(null);

    try {
      const data = moment("2020-11-17").format("YYYY-MM-DD");
      const meniuResponse = await getMeniuDetailsByDateAndLocation(
        data,
        locatie
      );
      setMeniuCantina(meniuResponse.data);
    } catch (error) {
      setError(error.response);
    }
    setIsLoading(false);
  }, []);

  useEffect(() => {
    if (props.setari !== null) {
      fetchData(props.setari.CantinaPreferata);
    }
  }, [fetchData, props.setari]);

  let content = null;

  content =
    meniuCantina.length === 0 ? (
      <Message info content="😕 Nu există meniu de afișat!" />
    ) : (
      <>
        {meniuCantina.map((item) => (
          <ItemCantinaComponent
            detaliiItem={item}
            key={item.ID_ArticolCantina}
          />
        ))}
      </>
    );

  if (error) {
    content = <ErrorMessageComponent error={error} />;
  }

  if (isLoading) {
    content = <LoaderMessageComponentElement />;
  }

  return content;
};

export default CantinaComponent;
