import React, { useEffect, useState } from "react";
import { Segment, Label, Grid } from "semantic-ui-react";

const ItemCantinaComponent = (props) => {
  const [itemValue, setItemValue] = useState({});

  useEffect(() => {
    if (props.detaliiItem !== null) {
      setItemValue(props.detaliiItem);
    }
  }, [props.detaliiItem]);

  return (
    <Segment style={{ borderRadius: "6px", width: "100%" }} textAlign={"left"}>
      <Grid>
        <Grid.Row columns={2}>
          <Grid.Column width={9} verticalAlign={"middle"}>
            {itemValue.Denumire + " - " + itemValue.Cantitate}
          </Grid.Column>
          <Grid.Column width={7} verticalAlign={"middle"}>
            <Label
              as="a"
              tag
              style={{ marginLeft: "2em" }}
              size={"small"}
            >
              {itemValue.Pret} lei
            </Label>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};

export default ItemCantinaComponent;
