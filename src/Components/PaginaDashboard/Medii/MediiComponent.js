import React, { useCallback, useEffect, useState } from "react";
import { mediiStudentAnUniversitarListByUsername } from "../../../Utils/ApiCalls";
import { Message } from "semantic-ui-react";
import ErrorMessageComponent from "../../GeneralElements/ErrorMessageComponent";
import LoaderMessageComponentElement from "../../GeneralElements/LoaderMessageComponentElement";
import { getUsername } from "../../../Utils/GetUsername";
import ItemMediiComponent from "./ItemMediiComponent";

const MediiComponent = (props) => {
  const [mediiStudent, setMediiStudent] = useState([]);
  const [error, setError] = useState(null); //folosit pentru afisarea mesajului de eroare daca nu vin datele de afisat
  const [isLoading, setIsLoading] = useState(false);

  const username = getUsername();

  const fetchData = useCallback(
    async (studentInfo) => {
      setIsLoading(true);
      setError(null);
      try {
        const mediiStudentResponse =
          await mediiStudentAnUniversitarListByUsername(username);

        let mediiListFiltered = [];
        for (let i = 0; i < mediiStudentResponse.data.length; i++) {
          if (
            mediiStudentResponse.data[i].ID_Student ===
              studentInfo.ID_Student &&
            mediiStudentResponse.data[i].MediaAnCuFacultative !== 0
          ) {
            mediiListFiltered.push(mediiStudentResponse.data[i]);
          }
        }

        setMediiStudent(mediiListFiltered.sort((a,b) => (a.ID_AnUniv < b.ID_AnUniv) ? 1 : ((b.ID_AnUniv < a.ID_AnUniv) ? -1 : 0)));

      } catch (error) {
        setError(error.response);
      }
      setIsLoading(false);
    },
    [username]
  );

  useEffect(() => {
    if (props.studentInfo !== null) {
      fetchData(props.studentInfo.info);
    }
  }, [fetchData, props.studentInfo]);

  let content = null;
  content =
    mediiStudent.length === 0 ? (
      <Message info content="😕 Nu există informații de afișat!" />
    ) : (
      <>
        {mediiStudent.map((item) => (
          <ItemMediiComponent detaliiItem={item} key={item.ID_AnUniv} />
        ))}
      </>
    );

  if (error) {
    content = <ErrorMessageComponent error={error} />;
  }

  if (isLoading) {
    content = <LoaderMessageComponentElement />;
  }

  return content;
};

export default MediiComponent;
