import React, { useEffect, useState } from "react";
import { Segment, Header, Grid } from "semantic-ui-react";

const ItemMediiComponent = (props) => {
  const [itemValue, setItemValue] = useState({});

  useEffect(() => {
    if (props.detaliiItem !== null) {
      setItemValue(props.detaliiItem);
    }
  }, [props.detaliiItem]);

  return (
    <Segment style={{ borderRadius: "6px" }} textAlign={"left"}>
      <Grid>
        <Grid.Row centered>
          <Grid.Column verticalAlign={"middle"}>
            <Header as="h5">{itemValue.DenumireAnUniv}</Header>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column textAlign="center">
            {`Situație școlară: ${itemValue.DenumireSituatieScolaraFINAL}`}
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column textAlign="center">
            {`🎯 Media finală: ${itemValue.MedieDacaIntegralist_An}`}
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={2}>
          <Grid.Column>
            {`Medie semestrul I: ${itemValue.MedieDacaIntegralist_S1}`}
          </Grid.Column>
          <Grid.Column>
            {`Număr de credite: ${itemValue.SumaCrediteMateriiPromovate_S1}`}
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={2}>
          <Grid.Column>
            {`Medie semestrul II: ${itemValue.MedieDacaIntegralist_S2}`}
          </Grid.Column>
          <Grid.Column>
            {`Număr de credite: ${itemValue.SumaCrediteMateriiPromovate_S2}`}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};

export default ItemMediiComponent;
