import React, { useCallback, useEffect, useState } from "react";
import { anuntSiteList } from "../../../Utils/ApiCalls";
import { Message } from "semantic-ui-react";
import ErrorMessageComponent from "../../GeneralElements/ErrorMessageComponent";
import LoaderMessageComponentElement from "../../GeneralElements/LoaderMessageComponentElement";
import ItemAnunturiComponent from "./ItemAnunturiComponent";
import { v4 as uuidv4 } from "uuid";

const AnunturiComponent = (props) => {
  const [anunturi, setAnunturi] = useState([]);
  const [error, setError] = useState(null); //folosit pentru afisarea mesajului de eroare daca nu vin datele de afisat
  const [isLoading, setIsLoading] = useState(false);

  const fetchData = useCallback(async (ID_Departament) => {
    setIsLoading(true);
    setError(null);
    try {
      //todo: de actualizat pt cand se va putea alege limba
      const anunturiResponse = await anuntSiteList(ID_Departament, true);
      setAnunturi(anunturiResponse.data);
    } catch (error) {
      setError(error.response);
    }
    setIsLoading(false);
  }, []);

  useEffect(() => {
    fetchData(props.ID_Departament);
  }, [fetchData, props.ID_Departament]);

  let content = null;
  content =
    anunturi.length === 0 ? (
      <Message info content="😕 Nu există anunțuri de afișat!" />
    ) : (
      <>
        {anunturi.map((item) => (
          <ItemAnunturiComponent
            detaliiItem={item}
            key={uuidv4()}
            screenWidthAnunturiComponent={props.screenWidth}
          />
        ))}
      </>
    );

  if (error) {
    content = <ErrorMessageComponent error={error} />;
  }

  if (isLoading) {
    content = <LoaderMessageComponentElement />;
  }

  return content;
};

export default AnunturiComponent;
