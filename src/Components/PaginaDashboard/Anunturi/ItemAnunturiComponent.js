import React, { useEffect, useState } from "react";
import { Segment, Header, Grid } from "semantic-ui-react";

const ItemAnunturiComponent = (props) => {
  const [itemValue, setItemValue] = useState({});

  useEffect(() => {
    if (props.detaliiItem !== null) {
      setItemValue(props.detaliiItem);
    }
  }, [props.detaliiItem]);

  const trimiteCatreAnunt = (e) => {
    const link = itemValue.LinkAnunt;
    window.open(link, "_black");
  };

  return (
    <Segment
      style={{ borderRadius: "6px", width: "100%" }}
      textAlign={"center"}
      onClick={trimiteCatreAnunt}
    >
      <Grid>
        <Grid.Row centered>
          <Grid.Column verticalAlign={"middle"}>
            <Header as="h5">{itemValue.TitluAnunt}</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column textAlign="justified">
            <div style={{textAlign: "center"}}
              dangerouslySetInnerHTML={{ __html: itemValue.ContinutAnunt }}
            ></div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};

export default ItemAnunturiComponent;
