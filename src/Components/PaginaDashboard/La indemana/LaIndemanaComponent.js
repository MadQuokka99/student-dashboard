import React from "react";
import { Grid } from "semantic-ui-react";
import Sigla from "../../../Utils/Sigla";
import ItemPlatformeUnitbvComponent from "../PlatformeUNITBV/ItemPlatformeUnitbvComponent";

const LaIndemanaComponent = (props) => {
  return (
    <Grid columns={2}>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"📃 Hotărâri CA"}
          link={"https://intranet.unitbv.ro/Documente/Hotarari-CA"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"🤚🏻 Hotărâri Senat"}
          link={
            "https://www.unitbv.ro/despre-unitbv/regulamente-si-hotarari/hotarari-senat.html"
          }
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"📜 An universitar"}
          link={
            "https://www.unitbv.ro/despre-unitbv/informatii-de-interes-public/structura-anului-universitar.html"
          }
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"📚 Baze de date academice"}
          link={"https://intranet.unitbv.ro/Facultate/Baze-de-date"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"🖋 Scriere academică"}
          link={"https://intranet.unitbv.ro/Documente/Scriere-academica"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          logo={<Sigla icon={"unitbv"} size="small" />}
          titlu={" Identitate vizuală UNITBV"}
          link={
            "https://intranet.unitbv.ro/Documente/Identitate-vizuala-UNITBV"
          }
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"📧 E-mailuri angajați"}
          link={"https://intranet.unitbv.ro/Servicii-IT/Personal-Universitate"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"🔧 Întrebări frecvente IT"}
          link={
            "https://intranet.unitbv.ro/Servicii-IT/%C3%8Entreb%C4%83ri-frecvente-IT"
          }
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
    </Grid>
  );
};

export default LaIndemanaComponent;
