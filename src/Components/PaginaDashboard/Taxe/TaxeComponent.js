import React, { useCallback, useEffect, useState } from "react";
import {
  rateTaxeStudent_Camin,
  rateTaxeStudent_RestulTaxelor,
} from "../../../Utils/ApiCalls";
import { Message } from "semantic-ui-react";
import ErrorMessageComponent from "../../GeneralElements/ErrorMessageComponent";
import LoaderMessageComponentElement from "../../GeneralElements/LoaderMessageComponentElement";
import { getUsername } from "../../../Utils/GetUsername";
import ItemTaxeComponent from "./ItemTaxeComponent";

const TaxeComponent = (props) => {
  const [taxeStudent, setTaxeStudent] = useState([]);
  const [error, setError] = useState(null); //folosit pentru afisarea mesajului de eroare daca nu vin datele de afisat
  const [isLoading, setIsLoading] = useState(false);

  const username = getUsername();

  const fetchData = useCallback(
    async (studentInfo) => {
      setIsLoading(true);
      setError(null);

      let taxeCamin = [];
      let taxe_RestulTaxelor = [];

      try {
        const taxeCaminStudentResponse = await rateTaxeStudent_Camin(
          username,
          studentInfo.ID_AnUniv
        );

        if (taxeCaminStudentResponse.data.length !== 0) {
          taxeCamin = taxeCaminStudentResponse.data;
        }

        const alteTaxeStudentResponse = await rateTaxeStudent_RestulTaxelor(
          username,
          studentInfo.ID_AnUniv
        );

        if (alteTaxeStudentResponse.data.length !== 0) {
          taxe_RestulTaxelor = alteTaxeStudentResponse.data;
        }

        Array.prototype.push.apply(taxeCamin, taxe_RestulTaxelor);
        setTaxeStudent(taxeCamin);
      } catch (error) {
        setError(error.response);
      }
      setIsLoading(false);
    },
    [username]
  );

  useEffect(() => {
    if (props.studentInfo !== null) {
      fetchData(props.studentInfo.info);
    }
  }, [fetchData, props.studentInfo]);

  let content = null;
  content =
    taxeStudent.length === 0 ? (
      <Message info content="🤑 Nu ai taxe de achitat!" />
    ) : (
      <>
        {taxeStudent.map((item) => (
          <ItemTaxeComponent detaliiItem={item} key={item.ID_TaxeStudent} />
        ))}
      </>
    );

  if (error) {
    content = <ErrorMessageComponent error={error} />;
  }

  if (isLoading) {
    content = <LoaderMessageComponentElement />;
  }

  return content;
};

export default TaxeComponent;
