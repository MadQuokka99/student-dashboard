import React, { useEffect, useState } from "react";
import { Segment, Header, Grid } from "semantic-ui-react";
import moment from "moment";

const ItemTaxeComponent = (props) => {
  const [itemValue, setItemValue] = useState({});

  useEffect(() => {
    if (props.detaliiItem !== null) {
      setItemValue(props.detaliiItem);
    }
  }, [props.detaliiItem]);

  return (
    <Segment style={{ borderRadius: "6px" }} textAlign={"left"}>
      <Grid stackable>
        <Grid.Row centered>
          <Grid.Column verticalAlign={"middle"}>
            <Header as="h5">{`${itemValue.DenumireIntreaga}`}</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={2}>
          <Grid.Column>{`💲 Valoare: ${itemValue.Valoare} ${itemValue.Moneda}`}</Grid.Column>
          <Grid.Column>
            {`📅 Perioadă: ${moment(itemValue.StartPerioada).format(
              "DD.MM.YYYY"
            )} - ${moment(itemValue.EndPerioada).format("DD.MM.YYYY")}`}
          </Grid.Column>

          <Grid.Column>
            {`😢 Penalizare: ${itemValue.SumaPenalizareDePlata} ${itemValue.Moneda}`}
          </Grid.Column>
          <Grid.Column>
            {`⏳ Scadență plată: ${moment(itemValue.DataScadenta).format(
              "DD.MM.YYYY"
            )}`}
          </Grid.Column>
          <Grid.Column>
            {`✅ Suma plătită: ${itemValue.SumaPlatita}`}
          </Grid.Column>
        </Grid.Row>
        <Grid.Row centered>
          <Grid.Column textAlign="center">{`💳 Rest de plată: ${itemValue.RestDePlata} ${itemValue.Moneda}`}</Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};

export default ItemTaxeComponent;
