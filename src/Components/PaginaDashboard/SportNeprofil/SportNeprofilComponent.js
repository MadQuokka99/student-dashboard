import React, { useCallback, useEffect, useState } from "react";
import { sportNeprofilGetByUsernameAnUniv } from "../../../Utils/ApiCalls";
import { Message } from "semantic-ui-react";
import ErrorMessageComponent from "../../GeneralElements/ErrorMessageComponent";
import LoaderMessageComponentElement from "../../GeneralElements/LoaderMessageComponentElement";
import { getUsername } from "../../../Utils/GetUsername";
import { v4 as uuidv4 } from "uuid";
import ItemSportNeprofilComponent from "./ItemSportNeprofilComponent";

const SportNeprofilComponent = (props) => {
  const [sportNeprofilList, setSportNeprofilList] = useState([]);
  const [error, setError] = useState(null); //folosit pentru afisarea mesajului de eroare daca nu vin datele de afisat
  const [isLoading, setIsLoading] = useState(false);

  const username = getUsername();

  const fetchData = useCallback(
    async (studentInfo) => {
      setIsLoading(true);
      setError(null);
      try {
        const sportNeprofilListResponse =
          await sportNeprofilGetByUsernameAnUniv(username);

        let sportNeprofilFilteredList = [];
        for (let i = 0; i < sportNeprofilListResponse.data.length; i++) {
          if (
            sportNeprofilListResponse.data[i].ID_Student ===
              studentInfo.info.ID_Student &&
            sportNeprofilListResponse.data[i].ID_Facultate ===
              studentInfo.info.ID_Facultate &&
            (studentInfo.info.DenumireAnStudiu === "I" ||
              studentInfo.info.DenumireAnStudiu === "II") &&
            studentInfo.info.DenumireCicluInv === "Licenta"
          ) {
            sportNeprofilFilteredList.push(sportNeprofilListResponse.data[i]);
          }
        }

        setSportNeprofilList(sportNeprofilFilteredList);
      } catch (error) {
        setError(error.response);
      }
      setIsLoading(false);
    },
    [username]
  );

  useEffect(() => {
    if (props.studentInfo !== null) {
      fetchData(props.studentInfo);
    }
  }, [fetchData, props.studentInfo]);

  let content = null;
  content =
    sportNeprofilList.length === 0 ? (
      <Message info content="😕 Nu există informații de afișat!" />
    ) : (
      <>
        {sportNeprofilList.map((item) =>
          item.ID_SportNeprofil === -1 ? (
            <Message info content="😕 Nu ți-ai ales sportul preferat!" />
          ) : (
            <ItemSportNeprofilComponent
              detaliiItem={item}
              key={
                item.ID_SportNeprofil === "-1"
                  ? uuidv4()
                  : item.ID_SportNeprofil
              }
            />
          )
        )}
      </>
    );

  if (error) {
    content = <ErrorMessageComponent error={error} />;
  }

  if (isLoading) {
    content = <LoaderMessageComponentElement />;
  }

  return content;
};

export default SportNeprofilComponent;
