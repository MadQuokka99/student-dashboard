import React, { useEffect, useState } from "react";
import { Segment, Header, Grid } from "semantic-ui-react";

const ItemSportNeprofilComponent = (props) => {
  const [itemValue, setItemValue] = useState({});

  const capitalizeFirstLetter = (string) => {
    let stringFormatted = null;
    if (string === undefined || string === "" || string === null) {
      stringFormatted = null;
    } else {
      stringFormatted = string.charAt(0).toUpperCase() + string.slice(1);
    }

    return stringFormatted;
  };

  useEffect(() => {
    if (props.detaliiItem !== null) {
      setItemValue(props.detaliiItem);
    }
  }, [props.detaliiItem]);

  return (
    <Segment style={{ borderRadius: "6px" }} textAlign={"left"}>
      <Grid>
        <Grid.Row centered>
          <Grid.Column verticalAlign={"middle"}>
            <Header as="h5">
              {capitalizeFirstLetter(itemValue.Disciplina)}
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column textAlign="justified">
            {`📅 Zi/zile: ${itemValue.Zi}`}
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>{`🤾🏻‍♂️ Cadru didactic: ${itemValue.CadruDidactic}`}</Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>{`✅ Număr prezențe: ${
            itemValue.Prezente === undefined ? "" : itemValue.Prezente.length
          }/${itemValue.NrPrezenteNecesar}`}</Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};

export default ItemSportNeprofilComponent;
