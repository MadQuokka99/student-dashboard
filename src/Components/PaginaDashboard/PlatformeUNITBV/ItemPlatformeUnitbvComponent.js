import React, { useEffect, useState } from "react";
import { Button } from "semantic-ui-react";

const ItemPlatformeUnitbvComponent = (props) => {
  const [logo, setLogo] = useState(null);
  const [titlu, setTitlu] = useState("");
  const [link, setLink] = useState("");

  useEffect(() => {
    if (props.logo !== undefined) {
      if (props.logo !== null) {
        setLogo(props.logo);
      }
    }

    if (props.titlu !== null) {
      setTitlu(props.titlu);
    }

    if (props.link !== null) {
      setLink(props.link);
    }
  }, [props.logo, props.titlu, props.link]);

  const handleClick = (e) => {
    window.open(link, "_black");
  };

  return (
    <>
      <Button
        compact
        onClick={handleClick}
        fluid
        type="button"
        style={{ height: props.screenWidth < 600 && "6.5vh" }}
      >
        {logo === undefined ? null : logo}
        {titlu}
      </Button>
    </>
  );
};

export default ItemPlatformeUnitbvComponent;
