import React from "react";
import Sigla from "../../../Utils/Sigla";
import { Grid } from "semantic-ui-react";
import ItemPlatformeUnitbvComponent from "./ItemPlatformeUnitbvComponent";

const PlatformeUnitbvComponent = (props) => {
  return (
    <Grid columns={2}>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          logo={<Sigla icon={"unitbv"} size="small" />}
          titlu={" UNITBV"}
          link={"https://www.unitbv.ro/"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"📚 E-learning"}
          link={"https://elearning.unitbv.ro/"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"📬 E-mail"}
          link={"https://student.unitbv.ro/"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"💻 Sesizări IT"}
          //todo: de actualizat sa se ia varianta in functie de limba
          link={"https://ticket.unitbv.ro/"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"🛫 Erasmus"}
          link={"https://erasmus.unitbv.ro/"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"🔧 Practică"}
          link={"https://practica.unitbv.ro/"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>

      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"💖 Consiliere"}
          link={"https://consiliere.unitbv.ro/"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"✍🏻 Scriere academică"}
          link={"https://writing.unitbv.ro/"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"🌎 Limbi străine"}
          link={"https://cilm.ro/"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"🙋🏻 Voluntariat"}
          link={"http://voluntar.unitbv.ro/"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"📈 Antreprenoriat"}
          link={"https://sas.unitbv.ro/"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"🎶 Radio"}
          link={"http://rct.unitbv.ro/"}
          screenWidth={props.screenWidth}
        />
      </Grid.Column>
      <Grid.Column>
        <ItemPlatformeUnitbvComponent
          titlu={"🙋🏻‍♂️ TSG"}
          link={"https://tsg.unitbv.ro/"}
          screenWidth={props.screenWidth}
          //todo: de pus logo TSG
          //logo={<img src="TSG.png" alt="" />}
        />
      </Grid.Column>
    </Grid>
  );
};

export default PlatformeUnitbvComponent;
