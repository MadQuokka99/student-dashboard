import React, { useEffect, useState } from "react";
import { Segment, Header, Grid } from "semantic-ui-react";
import moment from "moment";

const ItemAnunturiIntranetComponent = (props) => {
  const [itemValue, setItemValue] = useState({});
  const [mesajAnunt, setMesajAnunt] = useState("");


  function htmlDecode(input) {
    var doc = new DOMParser().parseFromString(input, "text/html");
    return doc.documentElement.textContent;
  }

  useEffect(() => {
    if (props.detaliiItem !== null) {
      setItemValue(props.detaliiItem);

      const mesajAnunt = props.detaliiItem.MesajAnunt;
      const mesajAnuntParsed = htmlDecode(mesajAnunt);

      setMesajAnunt(mesajAnuntParsed);
    }
  }, [props.detaliiItem]);

  return (
    <Segment style={{ borderRadius: "6px" }} textAlign={"left"}>
      <Grid>
        <Grid.Row centered>
          <Grid.Column verticalAlign={"middle"}>
            <Header as="h5">{itemValue.TitluAnunt}</Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column textAlign="justified">
            {itemValue.MesajAnunt === "" || itemValue.MesajAnunt === null ? (
              <p style={{ textAlign: "center" }}>
                👓 Verifică secțiunea de{" "}
                <span
                  style={{
                    color: "#4183c4",
                    textDecoration: "none",
                    cursor: "pointer",
                  }}
                >
                  avizier facultate{" "}
                </span>
                pentru mai multe detalii!
              </p>
            ) : (
              <div dangerouslySetInnerHTML={{ __html: mesajAnunt }}></div>
            )}
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column textAlign="justified">
            {moment(itemValue.DataPostare).format("DD.MM.YYYY h:mm a")}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};

export default ItemAnunturiIntranetComponent;
