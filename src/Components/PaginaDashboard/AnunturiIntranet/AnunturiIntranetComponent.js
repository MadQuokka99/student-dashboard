import React, { useCallback, useEffect, useState } from "react";
import {
  anUniversitarCurentGet,
  anunturiListByUsernameStudentAnUniv,
} from "../../../Utils/ApiCalls";
import { Message } from "semantic-ui-react";
import { getUsername } from "../../../Utils/GetUsername";
import ErrorMessageComponent from "../../GeneralElements/ErrorMessageComponent";
import LoaderMessageComponentElement from "../../GeneralElements/LoaderMessageComponentElement";
import ItemAnunturiIntranetComponent from "./ItemAnunturiIntranetComponent";
import moment from "moment";

const AnunturiIntranetComponent = (props) => {
  const [anunturiIntranet, setAnunturiIntranet] = useState([]);
  const [error, setError] = useState(null); //folosit pentru afisarea mesajului de eroare daca nu vin datele de afisat
  const [isLoading, setIsLoading] = useState(false);

  const username = getUsername();

  const fetchData = useCallback(
    async (studentInfo) => {
      setIsLoading(true);
      setError(null);

      const ID_AnUniv = studentInfo.ID_AnUniv;
      const anUnivCurent = await anUniversitarCurentGet();

      //se ia data de inceput a anului universitar curent ca fiind data de cand se iau anunturile
      const dataDeCand =
        moment().format("YYYY-MM-DD") <
        moment(anUnivCurent.data.DataInceputSem2).format("YYYY-MM-DD")
          ? moment(anUnivCurent.data.DataInceputAnUniv).format("YYYY-MM-DD")
          : moment(anUnivCurent.data.DataInceputSem2).format("YYYY-MM-DD");

      console.log(dataDeCand);

      try {
        const anunturiIntranetResponse =
          await anunturiListByUsernameStudentAnUniv(
            username,
            ID_AnUniv,
            dataDeCand
          );

        setAnunturiIntranet(anunturiIntranetResponse.data);
      } catch (error) {
        setError(error.response);
      }
      setIsLoading(false);
    },
    [username]
  );

  useEffect(() => {
    if (props.studentInfo !== null) {
      fetchData(props.studentInfo.info);
    }
  }, [fetchData, props.studentInfo]);

  let content = null;
  content =
    anunturiIntranet.length === 0 ? (
      <Message info content="😕 Nu există anunțuri de afișat!" />
    ) : (
      <>
        {anunturiIntranet.map((item) => (
          <ItemAnunturiIntranetComponent
            detaliiItem={item}
            key={item.IdAnunt}
            screenWidthAnunturiComponent={props.screenWidth}
          />
        ))}
      </>
    );

  if (error) {
    content = <ErrorMessageComponent error={error} />;
  }

  if (isLoading) {
    content = <LoaderMessageComponentElement />;
  }

  return content;
};

export default AnunturiIntranetComponent;
