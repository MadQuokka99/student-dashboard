export const HairStyle = [
  {
    key: 1,
    text: "longHair",
    value: "longHair",
  },
  {
    key: 2,
    text: "shortHair",
    value: "shortHair",
  },
  {
    key: 3,
    text: "eyepatch",
    value: "eyepatch",
  },
  {
    key: 4,
    text: "hat",
    value: "hat",
  },
  {
    key: 5,
    text: "hijab",
    value: "hijab",
  },
  {
    key: 6,
    text: "turban",
    value: "turban",
  },
  {
    key: 7,
    text: "bigHair",
    value: "bigHair",
  },
  {
    key: 8,
    text: "bob",
    value: "bob",
  },
  {
    key: 9,
    text: "bun",
    value: "bun",
  },
  {
    key: 10,
    text: "curly",
    value: "curly",
  },
  {
    key: 11,
    text: "curvy",
    value: "curvy",
  },
  {
    key: 12,
    text: "dreads",
    value: "dreads",
  },
  {
    key: 13,
    text: "frida",
    value: "frida",
  },
  {
    key: 14,
    text: "fro",
    value: "fro",
  },
  {
    key: 15,
    text: "shavedSides",
    value: "shavedSides",
  },
  {
    key: 16,
    text: "straight",
    value: "straight01",
  },
  {
    key: 17,
    text: "shortRound",
    value: "shortRound",
  },
  {
    key: 18,
    text: "shortWaved",
    value: "shortWaved",
  },
  {
    key: 19,
    text: "none",
    value: "none",
  },
];

export const HairColor = [
  {
    key: 1,
    text: "auburn",
    value: "auburn",
  },
  {
    key: 2,
    text: "black",
    value: "black",
  },
  {
    key: 3,
    text: "blonde",
    value: "blonde",
  },
  {
    key: 4,
    text: "blondeGolden",
    value: "blondeGolden",
  },
  {
    key: 5,
    text: "brownDark",
    value: "brownDark",
  },
  {
    key: 6,
    text: "brown",
    value: "brown",
  },
  {
    key: 7,
    text: "pastel",
    value: "pastel",
  },
  {
    key: 8,
    text: "pastelPink",
    value: "pastelPink",
  },
  {
    key: 9,
    text: "platinum",
    value: "platinum",
  },
  {
    key: 10,
    text: "red",
    value: "red",
  },
  {
    key: 11,
    text: "gray",
    value: "gray",
  },
  {
    key: 12,
    text: "silverGray",
    value: "silverGray",
  },
  {
    key: 13,
    text: "none",
    value: "none",
  },
];

export const Accessories = [
  {
    key: 1,
    text: "kurt",
    value: "kurt",
  },
  {
    key: 2,
    text: "sunglasses",
    value: "sunglasses",
  },
  {
    key: 3,
    text: "wayfarers",
    value: "wayfarers",
  },
  {
    key: 4,
    text: "none",
    value: "none",
  },
];

export const FacialHair = [
  {
    key: 1,
    text: "medium",
    value: "medium",
  },
  {
    key: 2,
    text: "beardMedium",
    value: "beardMedium",
  },
  {
    key: 3,
    text: "light",
    value: "light",
  },
  {
    key: 4,
    text: "beardLight",
    value: "beardLight",
  },
  {
    key: 5,
    text: "majestic",
    value: "majestic",
  },
  {
    key: 6,
    text: "beardMajestic",
    value: "beardMajestic",
  },
  {
    key: 7,
    text: "fancy",
    value: "fancy",
  },
  {
    key: 8,
    text: "moustaceFancy",
    value: "moustaceFancy",
  },
  {
    key: 9,
    text: "magnum",
    value: "magnum",
  },
  {
    key: 10,
    text: "moustacheMagnum",
    value: "moustacheMagnum",
  },
  {
    key: 11,
    text: "none",
    value: "none",
  },
];

export const Clothes = [
  {
    key: 1,
    text: "blazer",
    value: "blazer",
  },
  {
    key: 2,
    text: "blazerAndShirt",
    value: "blazerAndShirt",
  },
  {
    key: 3,
    text: "blazerAndSweater",
    value: "blazerAndSweater",
  },
  {
    key: 4,
    text: "sweater",
    value: "sweater",
  },
  {
    key: 5,
    text: "collarAndSweater",
    value: "collarAndSweater",
  },
  {
    key: 6,
    text: "shirt",
    value: "shirt",
  },
  {
    key: 7,
    text: "graphicShirt",
    value: "graphicShirt",
  },
  {
    key: 8,
    text: "shirtCrewNeck",
    value: "shirtCrewNeck",
  },
  {
    key: 9,
    text: "shirtScoopNeck",
    value: "shirtScoopNeck",
  },
  {
    key: 10,
    text: "shirtVNeck",
    value: "shirtVNeck",
  },
  {
    key: 11,
    text: "hoodie",
    value: "hoodie",
  },
  {
    key: 12,
    text: "overall",
    value: "overall",
  },
  {
    key: 13,
    text: "none",
    value: "none",
  },
];

export const ClothesColor = [
  {
    key: 1,
    text: "black",
    value: "black",
  },
  {
    key: 2,
    text: "blue",
    value: "blue",
  },
  {
    key: 3,
    text: "gray",
    value: "gray",
  },

  {
    key: 4,
    text: "heather",
    value: "heather",
  },
  {
    key: 5,
    text: "pastel",
    value: "pastel",
  },
  {
    key: 6,
    text: "pastelBlue",
    value: "pastelBlue",
  },

  {
    key: 7,
    text: "pastelGreen",
    value: "pastelGreen",
  },
  {
    key: 8,
    text: "pastelOrange",
    value: "pastelOrange",
  },
  {
    key: 9,
    text: "pastelRed",
    value: "pastelRed",
  },
  {
    key: 10,
    text: "pastelYellow",
    value: "pastelYellow",
  },
  {
    key: 11,
    text: "pink",
    value: "pink",
  },
  {
    key: 12,
    text: "red",
    value: "red",
  },
  {
    key: 13,
    text: "white",
    value: "white",
  },
  {
    key: 14,
    text: "none",
    value: "none",
  },
];

export const Eyes = [
  {
    key: 1,
    text: "close",
    value: "close",
  },
  {
    key: 2,
    text: "cry",
    value: "cry",
  },
  {
    key: 3,
    text: "close",
    value: "close",
  },
  {
    key: 4,
    text: "roll",
    value: "roll",
  },
  {
    key: 5,
    text: "xDizzy",
    value: "xDizzy",
  },
  {
    key: 6,
    text: "eyeRoll",
    value: "eyeRoll",
  },
  {
    key: 7,
    text: "happy",
    value: "happy",
  },
  {
    key: 8,
    text: "hearts",
    value: "hearts",
  },
  {
    key: 9,
    text: "side",
    value: "side",
  },
  {
    key: 10,
    text: "squint",
    value: "squint",
  },
  {
    key: 11,
    text: "surprised",
    value: "surprised",
  },
  {
    key: 12,
    text: "wink",
    value: "wink",
  },
  {
    key: 13,
    text: "winkWacky",
    value: "winkWacky",
  },
  {
    key: 14,
    text: "none",
    value: "none",
  },
];

export const EyesBrow = [
  {
    key: 1,
    text: "angry",
    value: "angry",
  },
  {
    key: 2,
    text: "flat",
    value: "flat",
  },
  {
    key: 3,
    text: "raised",
    value: "raised",
  },
  {
    key: 4,
    text: "raisedExcited",
    value: "raisedExcited",
  },
  {
    key: 5,
    text: "sad",
    value: "sad",
  },
  {
    key: 6,
    text: "sadConcerned",
    value: "sadConcerned",
  },
  {
    key: 7,
    text: "unibrow",
    value: "unibrow",
  },
  {
    key: 8,
    text: "up",
    value: "up",
  },
  {
    key: 9,
    text: "upDown",
    value: "upDown",
  },
  {
    key: 10,
    text: "frown",
    value: "frown",
  },
  {
    key: 11,
    text: "none",
    value: "none",
  },
];

export const Mouth = [
  {
    key: 1,
    text: "concerned",
    value: "concerned",
  },
  {
    key: 2,
    text: "disbelief",
    value: "disbelief",
  },
  {
    key: 3,
    text: "eating",
    value: "eating",
  },
  {
    key: 4,
    text: "grimace",
    value: "grimace",
  },
  {
    key: 5,
    text: "scream",
    value: "scream",
  },
  {
    key: 6,
    text: "sad",
    value: "sad",
  },
  {
    key: 7,
    text: "smile",
    value: "smile",
  },
  {
    key: 8,
    text: "serious",
    value: "serious",
  },
  {
    key: 9,
    text: "tongue",
    value: "tongue",
  },
  {
    key: 10,
    text: "twinkle",
    value: "twinkle",
  },
  {
    key: 11,
    text: "vomit",
    value: "vomit",
  },
  {
    key: 12,
    text: "none",
    value: "none",
  },
];

export const Skin = [
  {
    key: 1,
    text: "tanned",
    value: "tanned",
  },
  {
    key: 2,
    text: "yellow",
    value: "yellow",
  },
  {
    key: 3,
    text: "pale",
    value: "pale",
  },
  {
    key: 4,
    text: "light",
    value: "light",
  },
  {
    key: 5,
    text: "brown",
    value: "brown",
  },
  {
    key: 6,
    text: "darkBrown",
    value: "darkBrown",
  },
  {
    key: 7,
    text: "black",
    value: "black",
  },
  {
    key: 8,
    text: "none",
    value: "none",
  },
];
