import axiosAGSISAPI from "../Utils/axios-AGSIS-API";

export const studentInfo = (username, ID_AnUniv) => {
  return axiosAGSISAPI.get(
    `Student/ListStudentByUsernameAnUniv?username=${username}&id_anUniversitar=${ID_AnUniv}`
  );
};

export const anUniversitarCurentGet = () => {
  return axiosAGSISAPI.get(`AnUniversitar/AnUniversitarCurent`);
};

export const anUniversitar_Get = (ID_AnUniv) => {
  return axiosAGSISAPI.get(
    `AnUniversitar/AnUniversitar_Get?ID_AnUniv=${ID_AnUniv}`
  );
};

export const noteListByID_Student = (ID_Student) => {
  return axiosAGSISAPI.get(
    `Student/NoteListByIdStudent?ID_Student=${ID_Student}`
  );
};

export const programareExamenListByStudentAnUniv = (ID_Student, ID_AnUniv) => {
  return axiosAGSISAPI.get(
    `ProgramareExamen/ProgramareExamenListByStudentAnUniv?ID_Student=${ID_Student}&ID_AnUniv=${ID_AnUniv}`
  );
};

export const getOrarByStudent = (luna, an, grupa, subgrupa) => {
  return axiosAGSISAPI.get(
    `DashboardStudent/GetOrarStudent?month=${luna}&year=${an}&grupa=${grupa}&subGrupa=${subgrupa}`
  );
};

export const bursaListByStudentAnUniv = (username, ID_AnUniv) => {
  return axiosAGSISAPI.get(
    `Student/BursaListByStudentAnUniv?username=${username}&ID_AnUniv=${ID_AnUniv}`
  );
};

export const rateTaxeStudent_Camin = (username, ID_AnUniv) => {
  return axiosAGSISAPI.get(
    `Student/RateTaxeStudent_Camin?UsernameStudent=${username}&ID_AnUniv=${ID_AnUniv}`
  );
};

export const rateTaxeStudent_RestulTaxelor = (username, ID_AnUniv) => {
  return axiosAGSISAPI.get(
    `Student/RateTaxeStudent_RestulTaxelor?UsernameStudent=${username}&ID_AnUniv=${ID_AnUniv}`
  );
};

export const mediiStudentAnUniversitarListByUsername = (username) => {
  return axiosAGSISAPI.get(
    `Student/StudentAnUniversitarListByUsername?username=${username}`
  );
};

export const sportNeprofilGetByUsernameAnUniv = (username) => {
  return axiosAGSISAPI.get(
    `SportNeprofil/SportNeprofilGetByUsernameAnUniv?username=${username}`
  );
};

export const getMeniuDetailsByDateAndLocation = (dataCurenta, locatia) => {
  return axiosAGSISAPI.get(
    `MeniuDetails/GetMeniuDetailsByDateAndLocation?date=${dataCurenta}&location=${locatia}`
  );
};

export const anuntSiteList = (ID_Departament, EsteFeedRO) => {
  return axiosAGSISAPI.get(
    `InfoKioskApp/AnuntSiteList?ID_Departament=${ID_Departament}&EsteFeedRO=${EsteFeedRO}`
  );
};

export const anunturiListByUsernameStudentAnUniv = (username,
  ID_AnUniv,
  dataDeCand
) => {
  return axiosAGSISAPI.get(
    `Anunturi/AnunturiListByUsernameStudentAnUniv?UsernameStudent=${username}&ID_AnUniv=${ID_AnUniv}&AnunturiAfisateDupaData=${dataDeCand}`
  );
};

export const notificareList = () => {
  return axiosAGSISAPI.get(`AplicatieMobil/NotificareList`);
};

export const setariDashboardStudentGetByStudent = () => {
  return axiosAGSISAPI.get(
    `DashboardStudent/SetariDashboardStudentGetByStudent`
  );
};

export const setariDashboardStudentUpdate = (setari) => {
  return axiosAGSISAPI.post(
    `DashboardStudent/SetariDashboardStudentUpdate`,
    setari
  );
};
