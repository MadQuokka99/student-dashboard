import { toast } from "react-semantic-toasts";

/**
 *
 * @param fn functia apelata la intervalul de timp specificat
 * @param ms interval de timp masurat in ms
 * @returns {function(*): void}
 *
 * functia creeaza un timer care apeleaza functia fn o data la intervalul ms de timp
 */
 export function debounce(fn, ms) {
    let timer;
    return (_) => {
      clearTimeout(timer);
      timer = setTimeout((_) => {
        timer = null;
        fn.apply(this, arguments);
      }, ms);
    };
  }
  

export const showToast = (isError, mesaj) => {
  if (isError) {
    return toast({
      type: "error",
      icon: "warning",
      title: mesaj,
      time: 4000,
    });
  } else {
    toast({
      type: "success",
      icon: "check",
      title: mesaj,
      time: 4000,
    });
  }
};

export const getKeysOfObject = function (obj) {
  var keys = [];
  for (var key in obj) {
    keys.push(key);
  }
  return keys;
};