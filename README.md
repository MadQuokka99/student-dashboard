# Student Dashboard

This project was created to be the main page of students' Intranet platform of Transilvania University of Brașov. It consists into a dashboard that includes main aspects of the students' life such as grades, canteen's menu, events and news, shortcuts for the most important University's platforms and so on. 

This dashboard helps students to have a general perspective of the most important aspects of their Campus experience and to easily access them. 

The project was created with **ReactJS** and **JavaScript**.

Used **node packages**:
* [axios](https://www.npmjs.com/package/axios)
* [semantic-ui-react](https://react.semantic-ui.com/)
* [react-router-dom](https://v5.reactrouter.com/web/guides/quick-start)
* [moment](https://momentjs.com/)
* [styled-components](https://styled-components.com/)
* [fullcalendar](https://fullcalendar.io/)
* [react-semantic-toasts](https://www.npmjs.com/package/react-semantic-toasts) 

## Table of contents
1. [Installation](https://gitlab.com/agsis_team/paginaloginintranet/dashboard-student#installation)
2. [Usage](https://gitlab.com/agsis_team/paginaloginintranet/dashboard-student#usage)
3. [Roadmap](https://gitlab.com/agsis_team/paginaloginintranet/dashboard-student#roadmap)
4. [Acknowledgements](https://gitlab.com/agsis_team/paginaloginintranet/dashboard-student#acknowledgements)

## Installation

After the project was cloned, you have to run these scripts in the project directory (it requires to have installed [Node.js](https://nodejs.org/en/)):

```bash
npm install
```
Installs the Node Package Manager (NPM).

```bash
npm start
```
Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Usage

In the main page users can see the needed information divided into cards with intuitive titles.

<img src="https://gitlab.com/agsis_team/paginaloginintranet/dashboard-student/-/raw/main/src/main-page.jpg" width="700" />


Users also can customize their avatar, set their favourite canteen or choose the preferred faculty, by clicking on the settings button in the avatar card.

<img src="https://gitlab.com/agsis_team/paginaloginintranet/dashboard-student/-/raw/main/src/settings.jpg" width="700" />

## Roadmap

* Currently, the dashboard is available only in Romanian. One main goal is to **translate it into English** for foreign students.
* Another aspect that can be improved in the future is to create a more **customizable experience** for users:
    - including drag & drop cards to prioritize the information
    - showing only the cards that the user marks as preffered
* Also, another goal is to create a **dashboard for teachers**.

## Acknowledgements

This project was part of the project *INTUIT@UNITBV* in the *"Be central!" 2020-2021* competition of Transilvania University of Brașov.